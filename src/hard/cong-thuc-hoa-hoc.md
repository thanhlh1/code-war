<p>Cho một c&ocirc;ng thức h&oacute;a học (được đưa ra dưới dạng một chuỗi), trả về số lượng của mỗi nguy&ecirc;n tử.</p>

<p>Một ph&acirc;n tử được biểu diễn gồm t&ecirc;n nguy&ecirc;n tử v&agrave; số lượng nguy&ecirc;n tử.&nbsp;</p>

<p>T&ecirc;n nguy&ecirc;n tử lu&ocirc;n bắt đầu bằng một k&yacute; tự viết hoa, sau đ&oacute; c&oacute; thể l&agrave;&nbsp;chữ c&aacute;i viết thường hoặc kh&ocirc;ng c&oacute;.</p>

<p>Sau t&ecirc;n nguy&ecirc;n tử l&agrave; một số thể hiện số lượng nguy&ecirc;n tử. Số lượng c&oacute; thể c&oacute; hoặc kh&ocirc;ng tuy nhi&ecirc;n nếu số đ&oacute; l&agrave; 1, sẽ kh&ocirc;ng c&oacute; chữ số n&agrave;o theo sau. V&iacute; dụ, NaOH l&agrave; c&oacute; thể, nhưng Na1O1H1&nbsp;l&agrave; kh&ocirc;ng thể.</p>

<p>Hai c&ocirc;ng thức nối với nhau tạo ra một c&ocirc;ng thức kh&aacute;c. V&iacute; dụ, H2O2He3Mg4 cũng l&agrave; một c&ocirc;ng thức.</p>

<p>Một c&ocirc;ng thức được đặt trong ngoặc đơn v&agrave; một số đếm&nbsp;cũng l&agrave; một c&ocirc;ng thức. V&iacute; dụ: (H2O2) v&agrave; (H2O2)3 l&agrave; c&aacute;c c&ocirc;ng thức.</p>

<p>Cho một c&ocirc;ng thức, xuất ra số đếm của tất cả c&aacute;c phần tử dưới dạng một chuỗi dưới dạng sau: t&ecirc;n đầu ti&ecirc;n (theo thứ tự từ điển), theo sau l&agrave; số đếm của n&oacute; (nếu số đ&oacute; nhiều hơn 1), theo sau l&agrave; t&ecirc;n thứ hai (theo thứ tự từ điển&nbsp;), theo sau l&agrave; số đếm của n&oacute; (nếu số đ&oacute; lớn hơn 1), v.v.</p>

<p><strong>V&iacute; dụ 1:</strong><br />
Input:&nbsp;<br />
formula = &quot;NaOH&quot;<br />
Output: &quot;HNaO&quot;</p>

<p>Giải tr&igrave;nh:<br />
Tổng số phần tử l&agrave; {&#39;H&#39; : 1 ,&#39;Na&#39;: 1, &#39;O&#39;: 1}.</p>

<p>Ghi ch&uacute;:</p>

<p>-Tất cả c&aacute;c t&ecirc;n nguy&ecirc;n tử bao gồm c&aacute;c chữ c&aacute;i viết thường, ngoại trừ k&yacute; tự đầu ti&ecirc;n l&agrave; chữ hoa.<br />
-Độ d&agrave;i của c&ocirc;ng thức sẽ nằm trong phạm vi [1, 1000].<br />
-C&ocirc;ng thức sẽ chỉ bao gồm c&aacute;c chữ c&aacute;i, chữ số v&agrave; dấu ngoặc tr&ograve;n v&agrave; l&agrave; một c&ocirc;ng thức hợp lệ như được định nghĩa trong b&agrave;i to&aacute;n</p><br>*****<br><br>class Solution():<br>    def cong_thuc_hoa_hoc(self, form):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "NaOH"
<br>     Expected Output:   "HNaO"<br>Test case 2:<br>     Input:   "Mg(OH)2"
<br>     Expected Output:   "H2MgO2"<br>Test case 3:<br>     Input:   "Ca(OH)2"
<br>     Expected Output:   "CaH2O2"<br>Test case 4:<br>     Input:   "Fe(OH)3"
<br>     Expected Output:   "FeH3O3"<br><br>*****