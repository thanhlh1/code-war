<p>Cho một mảng c&aacute;c số nguy&ecirc;n, x&eacute;t c&aacute;c ph&acirc;n đoạn con k phần tử li&ecirc;n tiếp:<br />
Trả về số lớn nhất của mỗi ph&acirc;n đoạn</p>

<p>Input: nums[]: mảng c&aacute;c số nguy&ecirc;n</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; k: Số phần tử của ph&acirc;n đoạn con.<br />
Output: arr[]c&aacute;c số lớn nhất của mỗi ph&acirc;n đoạn</p>

<p>V&iacute; dụ 1 :&nbsp;<br />
Input: nums = [1,2,3,4,-1,2,1], k = 3<br />
Output: [3,4,4,4,2]<br />
Giải th&iacute;ch<br />
[1,2,3] : max = 3<br />
[2,3,4] : max = 4<br />
[3,4,-1]: max = 4<br />
[4,-1,2] : max = 4<br />
[-1,2,1] : max = 2</p>

<p><br />
V&iacute; dụ 2:<br />
Input: nums = [1,2,3,4], k = 4&nbsp;<br />
Output: [4]<br />
Giải th&iacute;ch: [1,2,3,4] max &nbsp;= 4<br />
&nbsp;</p><br>*****<br><br>class Solution():<br>    def cuc_tri_dia_phuong(self, nums, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3,4,-1,2,1]
3
<br>     Expected Output:   [3,4,4,4,2]<br>Test case 2:<br>     Input:   [1,2,3,4]
4
<br>     Expected Output:   [4]<br>Test case 3:<br>     Input:   [-58,35,-11,-91,40,-82,51,56,-34,-59]
2
<br>     Expected Output:   [35,35,-11,40,40,51,56,56,-34]<br>Test case 4:<br>     Input:   [37,31,-12,-47,-31,-96,-95,89,-25,-73,-97,9,-94,-17,83,81,-94,-72,66,46,-66,31,23,1,-94,51,17,-85,31,63,95,-8,-6,-44,-86,4,11,-66,-48,-26,-92,-49,19,24,-90,58,-16,43,55,41,-9,-100,-29,-32,-46,-21,-83,-73,-17,75,-65,34,53,-8,3,98,-41,2,-38,-65,7,-94,-55,28,-81,20,3,86,-50,5,-53,-87,-35,95,39,-79,32,38,50,66,-8,-69,-94,-70,-53,-77,-57,-91,32,7]
10
<br>     Expected Output:   [89,89,89,89,89,89,89,89,83,83,83,83,83,83,83,81,66,66,66,51,63,95,95,95,95,95,95,95,95,95,95,11,11,19,24,24,58,58,58,58,58,58,58,58,58,58,55,55,55,41,75,75,75,75,75,75,98,98,98,98,98,98,98,98,98,98,28,28,86,86,86,86,86,86,95,95,95,95,95,95,95,95,95,95,66,66,66,66,66,66,32]<br><br>*****