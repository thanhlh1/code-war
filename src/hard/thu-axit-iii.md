<p>Nam v&agrave; Tiệp đ&atilde; ch&aacute;n giải quyết x&iacute;ch m&iacute;ch bằng to&aacute;n học, h&ocirc;m nay, Nam th&aacute;ch đấu Tiệp về h&oacute;a học<br />
S&aacute;ng sớm đ&atilde; thấy Nam mang đến rất nhiều lọ dung dịch k&igrave; lạ, trong suốt như nhau.&nbsp;<br />
Nam cho rằng chỉ c&oacute; 1 lọ chứa axit mạnh, c&ograve;n lại đều l&agrave; nước<br />
Chỉ c&oacute; quỳ t&iacute;m để thử, mỗi lần thử c&oacute; thể sử dụng nhiều quỳ t&iacute;m v&agrave; t&iacute;m sau khi đổi m&agrave;u sẽ phải bỏ đi.<br />
H&atilde;y gi&uacute;p Tiệp t&igrave;m ra lọ n&agrave;o chứa axit&nbsp;<br />
Nếu giả sử c&oacute; n lọ, m lần thử th&igrave; mất &iacute;t nhất bao nhi&ecirc;u giấy quỳ t&iacute;m để thử</p>

<p><strong>Input: </strong>n, m<br />
&nbsp;&nbsp; &nbsp; &nbsp; n l&agrave; số lọ dung dịch<br />
&nbsp;&nbsp; &nbsp; &nbsp; m l&agrave; số lần được ph&eacute;p thử<br />
<strong>Output:</strong> Số quỳ t&iacute;m tối thiểu cần d&ugrave;ng</p>

<p><strong>V&iacute; dụ 1:</strong><br />
Input: n = 2, m = 1:<br />
Output: 1</p>

<p><strong>V&iacute; dụ 2:</strong><br />
Input: n = 4, m = 1;<br />
Output: 2</p>

<p><strong>Giải th&iacute;ch:</strong><br />
Đổ một &iacute;t dung dịch từ c&aacute;c lọ 1,2&nbsp;ra c&ugrave;ng thử.(KQ 1)<br />
Tương tự đổ dung dịch từ lọ 1,3 ra c&ugrave;ng thử.(KQ 2)<br />
C&oacute; 4 trường hợp xảy ra:<br />
&nbsp;&nbsp; &nbsp;KQ 1 v&agrave; KQ 2 đều đổi m&agrave;u =&gt;&gt;&nbsp;Lọ 1 l&agrave; axit<br />
&nbsp;&nbsp; &nbsp;Chỉ c&oacute; KQ 1 đổi m&agrave;u =&gt;&gt;&nbsp;Lọ 2 l&agrave; axit<br />
&nbsp;&nbsp; &nbsp;Chỉ c&oacute; KQ 2 đổi m&agrave;u =&gt;&gt;&nbsp;Lọ 3 l&agrave; axit<br />
&nbsp;&nbsp; &nbsp;Cả 2 KQ đều kh&ocirc;ng đổi m&agrave;u =&gt;&gt;&nbsp;Lọ 4 l&agrave; axit</p>

<p><strong>Note:</strong></p>

<p>Loại I: n &lt; 30, m = 1<br />
Loại II: n &lt; 1000, m = 1<br />
Loại III: n &lt; 1000, m t&ugrave;y &yacute;</p><br>*****<br><br>class Solution():<br>    def thu_axit_iii(self, n, m):<br><br><br>*****<br><br>Test case 1:<br>     Input:   2
1
<br>     Expected Output:   1<br>Test case 2:<br>     Input:   4
1
<br>     Expected Output:   2<br>Test case 3:<br>     Input:   10
1
<br>     Expected Output:   4<br>Test case 4:<br>     Input:   15
1
<br>     Expected Output:   4<br><br>*****