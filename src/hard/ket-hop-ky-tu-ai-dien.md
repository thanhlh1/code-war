<p>Cho hai chuỗi k&yacute; tự s v&agrave; p: s c&oacute; thể trống v&agrave; chỉ chứa c&aacute;c chữ c&aacute;i in thường a-z; p c&oacute; thể trống v&agrave; chỉ chứa c&aacute;c chữ c&aacute;i in thường a-z v&agrave; c&aacute;c k&yacute; tự &quot;?&quot; , &quot;*&quot;.<br />
Trong đ&oacute;:&nbsp;<br />
&quot;?&quot; ph&ugrave; hợp với bất kỳ k&yacute; tự đơn n&agrave;o.<br />
&quot;*&quot; ph&ugrave; hợp với bất kỳ chuỗi k&yacute; tự n&agrave;o (bao gồm cả chuỗi trống).<br />
Kiểm tra xem s v&agrave; p c&oacute; thể tr&ugrave;ng nhau hay kh&ocirc;ng. Nếu tr&ugrave;ng trả về true, nếu kh&ocirc;ng trả về false.</p>

<p><strong>V&iacute; dụ 1:</strong><br />
Input: &nbsp;s = &quot;rikkeisoft&quot; , p = &quot;*&quot;<br />
Output: &nbsp;true<br />
Giải th&iacute;ch: &quot;*&quot; khớp với bất kỳ chuỗi n&agrave;o.</p>

<p><strong>V&iacute; dụ 2:</strong><br />
Input: &nbsp;s = &quot;ricode&quot; , p = &quot;ricoo?&quot;<br />
Output: &nbsp;false<br />
Giải th&iacute;ch: &quot;?&quot; khớp với &quot;e&quot;, nhưng chữ c&aacute;i thứ năm l&agrave; &quot;o&quot; kh&ocirc;ng khớp với &quot;d&quot;.</p><br>*****<br><br>class Solution():<br>    def so_sanh_chuoi(self, s, p):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "rikkeisoft"
"*"
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   "ricode"
"ricoo?"
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   "cb"
"?a"
<br>     Expected Output:   false<br>Test case 4:<br>     Input:   "adceb"
"*a*b"
<br>     Expected Output:   true<br><br>*****