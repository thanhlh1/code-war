<p>Cho một số nguy&ecirc;n dương n, trả về số lượng của tất cả c&aacute;c hồ sơ tham dự c&oacute; thể c&oacute; độ d&agrave;i n, sẽ được coi l&agrave; phần thưởng. C&acirc;u trả lời c&oacute; thể rất lớn, h&atilde;y trả lại sau khi mod 10^9 + 7.</p>

<p>Một hồ sơ tham dự của học sinh l&agrave; một chuỗi chỉ chứa ba k&yacute; tự sau:</p>

<p>&#39;A&#39;: Vắng mặt.<br />
&#39;L&#39;: Muộn.<br />
&#39;P&#39;: Hiện tại.<br />
Một bản ghi được coi l&agrave; phần thưởng nếu n&oacute; kh&ocirc;ng chứa nhiều hơn một chữ &#39;A&#39; (vắng mặt) hoặc nhiều hơn hai chữ &#39;L&#39; (trễ) li&ecirc;n tục.</p>

<p>V&iacute; dụ 1:<br />
Đầu v&agrave;o: n = 2<br />
Đầu ra: 8<br />
Giải tr&igrave;nh:<br />
C&oacute; 8 hồ sơ với độ d&agrave;i 2 sẽ được coi l&agrave; phần thưởng:<br />
&quot;PP&quot;, &quot;AP&quot;, &quot;PA&quot;, &quot;LP&quot;, &quot;PL&quot;, &quot;AL&quot;, &quot;LA&quot;, &quot;LL&quot;<br />
Chỉ &quot;AA&quot; sẽ kh&ocirc;ng được coi l&agrave; xứng đ&aacute;ng do nhiều lần vắng mặt.<br />
Lưu &yacute;: Gi&aacute; trị của n sẽ kh&ocirc;ng vượt qu&aacute; 100.000.</p><br>*****<br><br>class Solution():<br>    def ho_so_di_hoc_cua_sinh_vien_ii(self, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   2
<br>     Expected Output:   8<br>Test case 2:<br>     Input:   1
<br>     Expected Output:   3<br>Test case 3:<br>     Input:   3
<br>     Expected Output:   19<br>Test case 4:<br>     Input:   4
<br>     Expected Output:   43<br><br>*****