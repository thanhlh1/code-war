<p>Cho một mảng A chỉ bao gồm 0 v&agrave; 1, bạn h&atilde;y chia mảng th&agrave;nh 3 phần kh&ocirc;ng trống sao cho tất cả c&aacute;c phần n&agrave;y biểu thị c&ugrave;ng một gi&aacute; trị nhị ph&acirc;n.</p>

<p>Nếu c&oacute; thể, bạn h&atilde;y trả lại gi&aacute; trị bất kỳ [i, j] n&agrave;o với i + 1 &lt; j(i l&agrave; index cuối của phần thứ nhất, j l&agrave; index cuối của phần thứ 2), sao cho:</p>

<p>A [0], A [1], ..., A [i] l&agrave; phần đầu ti&ecirc;n;</p>

<p>A [i + 1], A [i + 2], ..., A [j-1],A [j] l&agrave; phần thứ hai,</p>

<p>A [j + 1], ..., A [A.length - 1] l&agrave; phần thứ ba.</p>

<p>Tất cả ba phần c&oacute; gi&aacute; trị nhị ph&acirc;n bằng nhau.</p>

<p>Nếu kh&ocirc;ng thể, trả về [-1, -1].</p>

<p>Lưu &yacute;: to&agrave;n bộ phần tử trong mảng được t&aacute;ch đều được gh&eacute;p lại theo đ&uacute;ng thứ tự tạo th&agrave;nh chuỗi nhị ph&acirc;n. V&iacute; dụ: [1,1,0] đại diện cho số thập ph&acirc;n 6 ko phải 3. Ngo&agrave;i ra, c&aacute;c số 0 đứng đầu được cho ph&eacute;p, v&igrave; vậy [0,1,1] v&agrave; [1,1] đại diện cho c&ugrave;ng một gi&aacute; trị. &nbsp;</p>

<p>V&iacute; dụ 1:</p>

<p>&nbsp; &nbsp; Input: [1,0,1,0,1]</p>

<p>&nbsp; &nbsp; Output: [0,3]</p>

<p>&nbsp; &nbsp; Giải th&iacute;ch:</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; - phần một: 1</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; - phần hai: 0, 1</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; - phần ba: 0, 1</p>

<p>V&iacute; dụ 2:</p>

<p>&nbsp; &nbsp; Input: [1,1,0,1,1]</p>

<p>&nbsp; &nbsp; Output: [-1,-1] &nbsp;</p>

<p>Ghi ch&uacute;:</p>

<p>&nbsp; &nbsp; 3 &lt;= A.length &lt;= 30000</p>

<p>&nbsp; &nbsp; A[i] == 0 hoặc&nbsp;A[i] == 1</p><br>*****<br><br>class Solution():<br>    def mang_bang_nhau(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,0,1,0,1]
<br>     Expected Output:   [0,3]<br>Test case 2:<br>     Input:   [1,0,0,1,0]
<br>     Expected Output:   [-1,-1]<br>Test case 3:<br>     Input:   [1,0,0,0,1,1,1,1,1,1]
<br>     Expected Output:   [-1,-1]<br>Test case 4:<br>     Input:   [1,0,0,0,0,0,1,0,0,1]
<br>     Expected Output:   [0,7]<br><br>*****