<p>Cho một mảng A gồm c&aacute;c số nguy&ecirc;n kh&ocirc;ng &acirc;m, mảng đ&oacute; l&agrave; mảng vu&ocirc;ng nếu với mỗi cặp phần tử liền kề, tổng của ch&uacute;ng l&agrave; một số ch&iacute;nh phương.</p>

<p>Trả về số lượng ho&aacute;n vị của A l&agrave; mảng vu&ocirc;ng. Hai ho&aacute;n vị A1 v&agrave; A2 kh&aacute;c nhau khi v&agrave; chỉ khi c&oacute; một số chỉ số i sao cho A1[i] != A2[i].</p>

<p>V&iacute; dụ 1:</p>

<p>Đầu v&agrave;o: [1,17,8]<br />
Đầu ra: 2<br />
Giải th&iacute;ch:<br />
[1,8,17] v&agrave; [17,8,1] l&agrave; c&aacute;c ho&aacute;n vị hợp lệ.</p>

<p>V&iacute; dụ 2:</p>

<p>Đầu v&agrave;o: [2,2,2]<br />
Đầu ra: 1<br />
&nbsp;<br />
Ch&uacute; &yacute;:<br />
1. 1 &lt;= A. chiều d&agrave;i &lt;= 12<br />
2. 0 &lt;= A [i] &lt;= 1e9</p><br>*****<br><br>class Solution():<br>    def so_luong_mang_vuong(self, a):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,17,8]
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   [2,2,2]
<br>     Expected Output:   1<br>Test case 3:<br>     Input:   [1,17,8]
<br>     Expected Output:   2<br><br>*****