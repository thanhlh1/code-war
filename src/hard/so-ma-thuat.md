<p>Nam v&agrave; Tiệp chơi 1 tr&ograve; chơi: mỗi người chọn 1 con số A v&agrave; B, ai liệt k&ecirc; được nhiều số chia hết cho A hoặc B l&agrave; người chiến thắng<br />
H&atilde;y gi&uacute;p Nam chiến thắng Tiệp bằng c&aacute;ch viết 1 h&agrave;m trả về số thứ N chia hết cho A hoặc B</p>

<p><strong>Input:</strong> N, A, B<br />
<strong>Output:</strong> số thứ N chia hết cho A hoặc B<br />
Kết quả c&oacute; thể rất lớn, trả về N % (10^9 + 7)</p>

<p><strong>V&iacute; dụ 1:&nbsp;</strong><br />
Input: 1 &nbsp;2 &nbsp;3<br />
Output: 2<br />
Giải th&iacute;ch: c&aacute;c số chia hết c&oacute; 2 hoặc 3: 2,3,4,6,8,9</p>

<p><strong>V&iacute; dụ 2:&nbsp;</strong><br />
Input: 7 5 6<br />
Output: 20<br />
Giải th&iacute;ch : C&aacute;c số chia hết cho 5 hoặc 6: 5,6,10,12,15,18,20,24,25</p>

<p><strong>Note:</strong><br />
LVL I: n , a, b &lt; 100<br />
LVL&nbsp;II: n, a, b &lt; 10^ 9<br />
LVL&nbsp;III: n, a, b &lt; 10^9, time &lt; 0,1s</p><br>*****<br><br>class Solution():<br>    def chia_het_iii(self, n, a, b):<br><br><br>*****<br><br>Test case 1:<br>     Input:   1
2
3
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   41604
32
321
<br>     Expected Output:   1214080<br>Test case 3:<br>     Input:   20
111
23
<br>     Expected Output:   391<br>Test case 4:<br>     Input:   456789213
2
5
<br>     Expected Output:   761315355<br><br>*****