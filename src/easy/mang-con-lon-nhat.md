<p>Cho một số mảng số nguy&ecirc;n, t&igrave;m ph&acirc;n đoạn phụ liền kề (chứa &iacute;t nhất một số) c&oacute; tổng lớn nhất v&agrave; trả về tổng của n&oacute;.</p>

<p>V&iacute; dụ:</p>

<pre>
<strong>Đầu V&agrave;o :</strong> [-2,1,-3,4,-1,2,1,-5,4],
<strong>Đầu ra  :</strong> 6
<strong>Giải th&iacute;ch:</strong>&nbsp;[4,-1,2,1] c&oacute; tổng lớn nhất l&agrave; 6.</pre><br>*****<br><br>class Solution():<br>    def subArray(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [-2,1,-3,4,-1,2,1,-5,4]
<br>     Expected Output:   6<br>Test case 2:<br>     Input:   [1,2]
<br>     Expected Output:   3<br>Test case 3:<br>     Input:   [-2,1]
<br>     Expected Output:   1<br>Test case 4:<br>     Input:   [0,1,2,-3,0,0,-1,5]
<br>     Expected Output:   5<br><br>*****