<p>Trả về vị tr&iacute; đầu ti&ecirc;n xuất hiện của chuỗi&nbsp;c&nbsp;trong chuỗi s&nbsp;hoặc -1 nếu kh&ocirc;ng tồn tại.</p>

<p>Khi chuỗi c rỗng kết quả trả về l&agrave; 0.</p>

<p><strong><span style="background-color:#ffffff">V&iacute; dụ 1: </span></strong></p>

<pre>
&nbsp;   - Đầu v&agrave;o: s = &quot;hello&quot;, c = &quot;ll&quot;
    - Kết quả: 2</pre>

<p><strong>V&iacute; dụ 2: </strong></p>

<pre>
&nbsp;   - Đầu v&agrave;o: s = &quot;aaaa&quot;, c = &quot;bba&quot;
    - Kết quả: -1
</pre>

<p>&nbsp;</p><br>*****<br><br>class Solution():<br>    def strStr(self, s, c):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "hello"
"ll"
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   "aaaaa"
"bba"
<br>     Expected Output:   -1<br>Test case 3:<br>     Input:   "cdefgacb absdfsd !!!"
"!"
<br>     Expected Output:   17<br>Test case 4:<br>     Input:   "iiiii"
"i"
<br>     Expected Output:   0<br><br>*****