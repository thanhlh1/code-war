<p>Nam đi mua một mảnh đất ở phố cổ, nhưng anh chỉ th&iacute;ch những mảnh đất h&igrave;nh vu&ocirc;ng. Cho tọa&nbsp;độ 4 đỉnh h&atilde;y x&aacute;c định gi&uacute;p Nam mảnh đất đ&oacute; c&oacute; phải h&igrave;nh vu&ocirc;ng hay kh&ocirc;ng.</p>

<p>Tọa độ 1 đỉnh l&agrave; mảng gồm 2 số nguy&ecirc;n<br />
Input: ma trận 4x2 l&agrave; tọa độ của 4 đỉnh<br />
Output: true nếu l&agrave; h&igrave;nh vu&ocirc;ng, false nếu kh&ocirc;ng</p>

<p><br />
<strong>V&iacute; dụ:</strong></p>

<p>p1 = [2,1], p2 = [1,1], p3 = [2,0], p4 = [1,0]:<br />
Input: [[2,1],[1,1],[2,0],[1,0]]<br />
Output: true</p>

<p><strong>Note:&nbsp;</strong><br />
Tất cả c&aacute;c số nguy&ecirc;n đầu v&agrave;o nằm trong phạm vi [-5000, 5000].<br />
C&aacute;c tọa độ&nbsp;đầu v&agrave;o kh&ocirc;ng theo&nbsp;thứ tự.</p>

<p>&nbsp;</p><br>*****<br><br>class Solution():<br>    def manh_dat_hinh_vuong(self, matrix):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [[2,1],[1,1],[2,0],[1,0]]
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   [[1,0],[1,1],[1,2],[0,1]]
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   [[0,0],[1,1],[1,0],[0,1]]
<br>     Expected Output:   true<br>Test case 4:<br>     Input:   [[3,3],[-1,1],[2,0],[1,0]]
<br>     Expected Output:   false<br><br>*****