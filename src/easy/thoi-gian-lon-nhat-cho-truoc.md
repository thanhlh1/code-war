<p>Cho một mảng gồm 4 chữ số, trả về thời gian 24 giờ lớn nhất c&oacute; thể được tạo th&agrave;nh</p>

<p>Thời gian 24 giờ nhỏ nhất l&agrave; 00:00 v&agrave; lớn nhất l&agrave; 23:59. Bắt đầu từ 00:00 v&agrave; kết th&uacute;c ở 23:59.</p>

<p>Trả về c&acirc;u trả lời dưới dạng một chuỗi c&oacute; độ d&agrave;i 5. Nếu kh&ocirc;ng c&oacute; thời gian hợp lệ n&agrave;o c&oacute; thể được thực hiện, h&atilde;y trả về một chuỗi trống.</p>

<p>&nbsp;</p>

<p>V&iacute; dụ 1:</p>

<p>&nbsp; &nbsp; Đầu v&agrave;o: [5,1,1,0]<br />
&nbsp; &nbsp; Đầu ra: &quot;15:10&quot;</p>

<p><br />
V&iacute; dụ 2:</p>

<p>&nbsp; &nbsp; Đầu v&agrave;o: [3,5,9,7]<br />
&nbsp; &nbsp; Đầu ra: &quot;&quot;<br />
&nbsp;</p>

<p>Ch&uacute; th&iacute;ch:</p>

<p>A.length == 4<br />
0 &lt;= A [i] &lt;= 9</p><br>*****<br><br>class Solution():<br>    def thoi_gian_lon_nhat_cho_truoc(self, A):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [5,1,1,0]
<br>     Expected Output:   "15:10"<br>Test case 2:<br>     Input:   [3,5,9,7]
<br>     Expected Output:   ""<br>Test case 3:<br>     Input:   [0,0,0,0]
<br>     Expected Output:   "00:00"<br>Test case 4:<br>     Input:   [0,0,1,0]
<br>     Expected Output:   "10:00"<br><br>*****