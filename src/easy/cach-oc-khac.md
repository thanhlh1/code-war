<p>Một ph&eacute;p m&atilde; h&oacute;a&nbsp;được định nghĩa như sau:</p>

<p>Bắt đầu từ số&nbsp;đầu ti&ecirc;n X. Với c&aacute;c số giống nhau th&igrave; nh&oacute;m lại th&agrave;nh một nh&oacute;m&nbsp;(giả sử c&oacute; Y số) th&igrave; d&atilde;y n&agrave;y được chuyển th&agrave;nh YX.</p>

<p>V&iacute; dụ 122344111 đọc l&agrave; &quot;một 1, hai 2, một 3, hai 4, ba 1&quot;, do đ&oacute; chuỗi 122344111 chuyển th&agrave;nh 1122132431.</p>

<p>Input:</p>

<p>Một&nbsp;chuỗi s cần m&atilde; h&oacute;a&nbsp;(kh&ocirc;ng qu&aacute; 1000 chữ số)</p>

<p>Output<br />
Một chuỗi l&agrave;&nbsp;kết quả của ph&eacute;p n&agrave;y</p>

<p><strong>V&iacute; dụ 1:</strong></p>

<p>Input:&nbsp;&quot;111&quot;</p>

<p>Output: &quot;31&quot;</p>

<p><strong>V&iacute; dụ 2:</strong></p>

<p>Input: &quot;12223&quot;</p>

<p>Output : &quot;113213&quot;</p><br>*****<br><br>class Solution():<br>    def ma_hoa(self, s):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "111"
<br>     Expected Output:   "31"<br>Test case 2:<br>     Input:   "12223"
<br>     Expected Output:   "113213"<br>Test case 3:<br>     Input:   "123123111133334144444414141414142144"
<br>     Expected Output:   "111213111213414314116411141114111411141114121124"<br><br>*****