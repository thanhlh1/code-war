<p>Cho một nh&oacute;m gồm hai chuỗi, bạn cần t&igrave;m độ d&agrave;i chuỗi con d&agrave;i nhất kh&ocirc;ng phổ biến của nh&oacute;m hai chuỗi n&agrave;y.</p>

<p>Chuỗi con kh&ocirc;ng phổ biến d&agrave;i nhất được định nghĩa l&agrave; chuỗi con d&agrave;i nhất của một trong c&aacute;c chuỗi n&agrave;y v&agrave; n&oacute; kh&ocirc;ng l&agrave; chuỗi con của chuỗi c&ograve;n lại.<br />
&nbsp;<br />
Bất kỳ chuỗi n&agrave;o cũng l&agrave; một chuỗi con của ch&iacute;nh n&oacute; v&agrave; một chuỗi rỗng l&agrave;&nbsp;chuỗi con của bất kỳ chuỗi n&agrave;o.<br />
&nbsp;<br />
&nbsp;Đầu v&agrave;o sẽ l&agrave; hai chuỗi v&agrave; đầu ra&nbsp;l&agrave; độ d&agrave;i của chuỗi con kh&ocirc;ng phổ biến d&agrave;i nhất. Nếu chuỗi con kh&ocirc;ng phổ biến d&agrave;i nhất kh&ocirc;ng tồn tại, trả về -1.<br />
&nbsp;<br />
&nbsp;V&iacute; dụ 1:<br />
&nbsp;Đầu v&agrave;o: &quot;aba&quot;, &quot;cdc&quot;<br />
&nbsp;Đầu ra: 3<br />
&nbsp;Giải th&iacute;ch: Chuỗi con kh&ocirc;ng phổ biến d&agrave;i nhất l&agrave; &quot;aba&quot; (hoặc &quot;cdc&quot;),<br />
&nbsp;bởi v&igrave; &quot;aba&quot; l&agrave; một chuỗi con&nbsp;của &quot;aba&quot;,<br />
&nbsp;nhưng kh&ocirc;ng phải l&agrave; một chuỗi con của chuỗi c&ograve;n lại.<br />
&nbsp;Ch&uacute; th&iacute;ch:<br />
&nbsp;<br />
&nbsp;Độ d&agrave;i của cả hai chuỗi sẽ kh&ocirc;ng vượt qu&aacute; 100.<br />
&nbsp;Chỉ c&aacute;c chữ c&aacute;i từ a ~ z sẽ xuất hiện trong chuỗi đầu v&agrave;o.</p><br>*****<br><br>class Solution():<br>    def day_con_khong_chung_dai_nhat(self, a, b):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "aba"
"cdc"
<br>     Expected Output:   3<br>Test case 2:<br>     Input:   ""
""
<br>     Expected Output:   -1<br>Test case 3:<br>     Input:   "abcdjfkjdkf"
"jfkjdkf"
<br>     Expected Output:   11<br>Test case 4:<br>     Input:   "aba"
"aba"
<br>     Expected Output:   -1<br><br>*****