<p>Bạn l&agrave; một t&ecirc;n trộm&nbsp;chuy&ecirc;n nghiệp l&ecirc;n kế hoạch trộm c&aacute;c ng&ocirc;i&nbsp;nh&agrave; dọc một con phố. Mỗi ng&ocirc;i nh&agrave; c&oacute; một số tiền nhất định được cất giấu, hạn chế duy nhất ngăn bạn cướp từng ng&ocirc;i nh&agrave; đ&oacute; l&agrave; những ng&ocirc;i nh&agrave; liền kề c&oacute; hệ thống an ninh được kết nối v&agrave; n&oacute; sẽ tự động li&ecirc;n lạc với cảnh s&aacute;t nếu hai ng&ocirc;i nh&agrave; liền kề bị đột nhập v&agrave;o c&ugrave;ng một đ&ecirc;m.</p>

<p>Đưa ra một danh s&aacute;ch c&aacute;c số nguy&ecirc;n kh&ocirc;ng &acirc;m biểu thị số tiền của mỗi ng&ocirc;i nh&agrave;, h&atilde;y x&aacute;c định số tiền tối đa bạn c&oacute; thể trộm&nbsp;tối nay m&agrave; kh&ocirc;ng b&aacute;o cho cảnh s&aacute;t.</p>

<p>V&iacute; dụ 1:</p>

<p>&nbsp; &nbsp; <strong>Đầu v&agrave;o</strong>: [1,2,3,1]</p>

<p>&nbsp; &nbsp; <strong>Đầu ra</strong>: 4</p>

<p>&nbsp; &nbsp; <strong>Giải th&iacute;ch</strong>: Trộm&nbsp;nh&agrave; 1 (tiền = 1) rồi trộm&nbsp;nh&agrave; 3 (tiền = 3).</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tổng số tiền bạn c&oacute; thể trộm&nbsp;= 1 + 3 = 4.</p>

<p>V&iacute; dụ 2:</p>

<p>&nbsp; &nbsp; <strong>Đầu v&agrave;o</strong>: [2,7,9,3,1]</p>

<p>&nbsp; &nbsp; <strong>Đầu ra</strong>: 12</p>

<p>&nbsp; &nbsp; <strong>Giải th&iacute;ch</strong>: Trộm&nbsp;nh&agrave; 1 (tiền = 2), trộm&nbsp;nh&agrave; 3 (tiền = 9) v&agrave; trộm&nbsp;nh&agrave; 5 (tiền = 1). &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Tổng số tiền bạn c&oacute; thể trộm&nbsp;= 2 + 9 + 1 = 12.</p><br>*****<br><br>class Solution():<br>    def ten_trom_thong_minh(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3,1]
<br>     Expected Output:   4<br>Test case 2:<br>     Input:   [2,7,9,3,1]
<br>     Expected Output:   12<br><br>*****