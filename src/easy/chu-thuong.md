<p>C&agrave;i đặt h&agrave;m ToLowerCase() với tham số truyền v&agrave;o kiểu String, trả về chuỗi lowercase của n&oacute;.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>&quot;Rikkei&quot;
<strong>Đầu ra: </strong>&quot;rikkei&quot;</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>&quot;GD&quot;
<strong>Đầu ra: </strong>&quot;gd&quot;</pre><br>*****<br><br>class Solution():<br>    def toLowerCase(self, str):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "Rikkei"
<br>     Expected Output:   "rikkei"<br>Test case 2:<br>     Input:   "pP"
<br>     Expected Output:   "pp"<br>Test case 3:<br>     Input:   "pPodndJPHJjpJpjpjjppjpJJPj"
<br>     Expected Output:   "ppodndjphjjpjpjpjjppjpjjpj"<br>Test case 4:<br>     Input:   "pPodndJAAAApjjppjpJJPj"
<br>     Expected Output:   "ppodndjaaaapjjppjpjjpj"<br><br>*****