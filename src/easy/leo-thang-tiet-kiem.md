<p>&nbsp;</p>

<p>Tr&ecirc;n cầu thang, bước thứ i c&oacute; một số chi ph&iacute; kh&ocirc;ng &acirc;m [i] được chỉ định (index từ 0).</p>

<p>Khi bạn trả chi ph&iacute;, bạn c&oacute; thể leo l&ecirc;n một hoặc hai bước.</p>

<p>Bạn cần t&igrave;m chi ph&iacute; tối thiểu để đạt đến đỉnh của thang v&agrave; bạn c&oacute; thể bắt đầu từ bước với chỉ số 0 hoặc bước với chỉ số 1.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> cost = [10, 15, 20]
<strong>Đầu ra:</strong> 15
<strong>Giải th&iacute;ch:</strong> Rẻ nhất l&agrave; bắt đầu ở cost[1], trả ph&iacute; đ&oacute; v&agrave; đi đến đỉnh.
</pre>

<p>&nbsp;</p>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
<strong>Đầu ra:</strong> 6
<strong>Giải th&iacute;ch:</strong> Bắt đầu ở cost[0], chỉ đi 1 bước, bỏ qua cost[3].</pre><br>*****<br><br>class Solution():<br>    def minCost(self, cost):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [10,15,20]
<br>     Expected Output:   15<br>Test case 2:<br>     Input:   [1,100,1,1,1,100,1,1,100,1]
<br>     Expected Output:   6<br>Test case 3:<br>     Input:   [0,0,0,0]
<br>     Expected Output:   0<br>Test case 4:<br>     Input:   [0,0,1,0]
<br>     Expected Output:   0<br><br>*****