<p>Kết hợp hai danh s&aacute;ch li&ecirc;n kết đ&atilde; sắp xếp v&agrave; trả về dưới dạng danh s&aacute;ch mới.</p>

<p>Danh s&aacute;ch mới n&ecirc;n được thực hiện bằng c&aacute;ch nối c&aacute;c n&uacute;t của hai danh s&aacute;ch đầu ti&ecirc;n.</p>

<p>Đầu ra l&agrave; một danh s&aacute;ch li&ecirc;n kết đ&atilde; được sắp xếp.</p>

<p>V&iacute; dụ: Đầu v&agrave;o: danh s&aacute;ch 1:&nbsp;1-&gt; 2-&gt; 4,</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;danh s&aacute;ch 2: 1-&gt; 3-&gt; 4</p>

<p>Đầu ra: 1-&gt; 1-&gt; 2-&gt; 3-&gt; 4-&gt; 4</p><br>*****<br><br># class ListNode:<br>#        def __init__(self, x):<br>#            self.val = x<br>#            self.next = None<br>class Solution():<br>    def noi_danh_sach(self, l1, l2):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,4]
[1,3,4]
<br>     Expected Output:   [1,1,2,3,4,4]<br>Test case 2:<br>     Input:   []
[1,2,4]
<br>     Expected Output:   [1,2,4]<br>Test case 3:<br>     Input:   [1,2,4]
[]
<br>     Expected Output:   [1,2,4]<br>Test case 4:<br>     Input:   [0,0,0,0,0,0]
[-1,0,1]
<br>     Expected Output:   [-1,0,0,0,0,0,0,0,1]<br><br>*****