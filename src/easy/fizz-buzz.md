<p>&nbsp;</p>

<p>Viết chương tr&igrave;nh đưa ra biểu diễn chuỗi của c&aacute;c số từ 1 đến n.</p>

<p>Nhưng đối với bội số của ba th&igrave; n&ecirc;n xuất ra &quot;Fizz&quot; thay v&igrave; số lượng v&agrave; cho bội số của năm đầu ra &quot;Buzz&quot;.</p>

<p>Đối với c&aacute;c số l&agrave; bội số của cả ba v&agrave; năm đầu ra &quot;FizzBuzz&quot;.</p>

<p><strong>V&iacute; dụ:</strong></p>

<pre>
n = 15,

Trả về:
[
    &quot;1&quot;,
    &quot;2&quot;,
    &quot;Fizz&quot;,
    &quot;4&quot;,
    &quot;Buzz&quot;,
    &quot;Fizz&quot;,
    &quot;7&quot;,
    &quot;8&quot;,
    &quot;Fizz&quot;,
    &quot;Buzz&quot;,
    &quot;11&quot;,
    &quot;Fizz&quot;,
    &quot;13&quot;,
    &quot;14&quot;,
    &quot;FizzBuzz&quot;
]</pre><br>*****<br><br>class Solution():<br>    def fizz_buzz(self, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   15
<br>     Expected Output:   ["1","2","Fizz","4","Buzz","Fizz","7","8","Fizz","Buzz","11","Fizz","13","14","FizzBuzz"]<br>Test case 2:<br>     Input:   1
<br>     Expected Output:   ["1"]<br>Test case 3:<br>     Input:   3
<br>     Expected Output:   ["1","2","Fizz"]<br>Test case 4:<br>     Input:   33
<br>     Expected Output:   ["1","2","Fizz","4","Buzz","Fizz","7","8","Fizz","Buzz","11","Fizz","13","14","FizzBuzz","16","17","Fizz","19","Buzz","Fizz","22","23","Fizz","Buzz","26","Fizz","28","29","FizzBuzz","31","32","Fizz"]<br><br>*****