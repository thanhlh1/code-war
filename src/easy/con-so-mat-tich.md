<p>Cho một mảng chứa n số ri&ecirc;ng biệt được lấy từ 0, 1, 2, ..., n, t&igrave;m một số bị thiếu trong mảng.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [3,0,1]
<strong>Đầu ra:</strong> 2
</pre>

<p><strong>V&iacute; dụ &nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [9,6,4,2,3,5,7,0,1]
<strong>Đầu ra:</strong> 8</pre><br>*****<br><br>class Solution():<br>    def missingNumber(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [3,0,1]
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   [9,6,4,2,3,5,7,0,1]
<br>     Expected Output:   8<br>Test case 3:<br>     Input:   [0,1,3,4,5,6,7,8,9,10,11,12,13,14,15]
<br>     Expected Output:   2<br>Test case 4:<br>     Input:   [0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,2]
<br>     Expected Output:   16<br><br>*****