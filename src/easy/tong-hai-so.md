<p>T&iacute;nh tổng 2 số a v&agrave; b nhưng kh&ocirc;ng d&ugrave;ng to&aacute;n tử +, -.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>a = 1, b = 2
<strong>Đầu ra: </strong>3
</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>a = -2, b = 3
<strong>Đầu ra: </strong>1</pre><br>*****<br><br>class Solution():<br>    def getSum(self, num1, num2):<br><br><br>*****<br><br>Test case 1:<br>     Input:   1
2
<br>     Expected Output:   3<br>Test case 2:<br>     Input:   -2
3
<br>     Expected Output:   1<br>Test case 3:<br>     Input:   55
-66
<br>     Expected Output:   -11<br>Test case 4:<br>     Input:   123456789
123456789
<br>     Expected Output:   246913578<br><br>*****