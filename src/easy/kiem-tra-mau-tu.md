<p>Cho một chuỗi partern v&agrave; một chuỗi str.</p>

<p>Bạn cần viết một chương tr&igrave;nh để kiểm tra sự tương đồng giữa pattern v&agrave; str.</p>

<p>H&agrave;m trả về true nếu mỗi k&iacute; tự trong pattern thể hiện một từ trong str.</p>

<p>Bạn c&oacute; thể giả sử: pattern chỉ gồm c&aacute;c chữ thường v&agrave; str cũng chỉ gồm c&aacute;c chữ in thường v&agrave; ngăn c&aacute;ch nhau bởi 1 k&iacute; tự khoảng trắng</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> pattern = <code>&quot;abba&quot;</code>, str = <code>&quot;dog cat cat dog&quot;</code>
<strong>Đầu ra:</strong> true</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong>pattern = <code>&quot;abba&quot;</code>, str = <code>&quot;dog cat cat fish&quot;</code>
<strong>Đầu ra:</strong> false</pre><br>*****<br><br>class Solution():<br>    def wordPattern(self, pattern, str):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "abba"
"dog cat cat dog"
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   "abba"
"dog cat cat fish"
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   "aaaa"
"dog cat cat fish"
<br>     Expected Output:   false<br><br>*****