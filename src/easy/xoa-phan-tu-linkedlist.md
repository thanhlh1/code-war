<p>X&oacute;a tất cả c&aacute;c th&agrave;nh phần khỏi danh s&aacute;ch li&ecirc;n kết số nguy&ecirc;n c&oacute; gi&aacute; trị val.</p>

<p>Th&iacute; dụ:</p>

<p>Đầu v&agrave;o: 1-&gt; 2-&gt; 6-&gt; 3-&gt; 4-&gt; 5-&gt; 6, val = 6<br />
Đầu ra: 1-&gt; 2-&gt; 3-&gt; 4-&gt; 5</p><br>*****<br><br># class ListNode:<br>#        def __init__(self, x):<br>#            self.val = x<br>#            self.next = None<br>class Solution():<br>    def delete_element_linkedlist(self, head, val):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,6,3,4,5,6]
6
<br>     Expected Output:   [1,2,3,4,5]<br>Test case 2:<br>     Input:   [1,5,2,6,3,4,5,6]
2
<br>     Expected Output:   [1,5,6,3,4,5,6]<br>Test case 3:<br>     Input:   [1,5,2,6,3,4,5,6]
5
<br>     Expected Output:   [1,2,6,3,4,6]<br>Test case 4:<br>     Input:   [1,5,2,6,3,4,5,6]
3
<br>     Expected Output:   [1,5,2,6,4,5,6]<br><br>*****