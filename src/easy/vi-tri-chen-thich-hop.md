<p>Cho một mảng đ&atilde; được sắp xếp từ nhỏ đến lớn&nbsp;v&agrave; một gi&aacute; trị target,.</p>

<p>Trả về vị tr&iacute; của phần tử c&oacute; gi&aacute; trị bằng với gi&aacute; trị target&nbsp;trong mảng.</p>

<p>Nếu kh&ocirc;ng tồn tại gi&aacute; trị target trong mảng th&igrave; trả về vị tr&iacute; của&nbsp;target sau khi được ch&egrave;n v&agrave;o mảng&nbsp;m&agrave; vẫn đảm bảo mảng được sắp xếp.</p>

<p>Bạn c&oacute; thể giả định kh&ocirc;ng c&oacute; gi&aacute; trị tr&ugrave;ng lặp trong mảng.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [1,3,5,6], 5
<strong>Đầu ra:</strong> 2
</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [1,3,5,6], 2
<strong>Đầu ra:</strong> 1
</pre>

<p><strong>V&iacute; dụ&nbsp;3:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [1,3,5,6], 7
<strong>Đầu ra:</strong> 4</pre><br>*****<br><br>class Solution():<br>    def searchInsert(self, nums, target):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,3,5,6]
5
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   [1,3,5,6]
2
<br>     Expected Output:   1<br>Test case 3:<br>     Input:   [1,3,5,6]
7
<br>     Expected Output:   4<br>Test case 4:<br>     Input:   [1,3,5,6]
0
<br>     Expected Output:   0<br><br>*****