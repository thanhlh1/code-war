<p>Cho 2 string s v&agrave; t v&agrave; chỉ bao gồm c&aacute;c k&yacute; tự thường.</p>

<p>String t được tạo ra ngẫu nhi&ecirc;n từ String s v&agrave; th&ecirc;m một k&yacute; tự ở vị tr&iacute; bất kỳ.</p>

<p>H&atilde;y t&igrave;m ra k&yacute; tự được th&ecirc;m v&agrave;o String t.</p>

<p><strong>V&iacute; dụ:</strong></p>

<pre>
Đầu v&agrave;o:
s = &quot;abcd&quot;
t = &quot;abcde&quot;

Đầu ra:
e

Giải th&iacute;ch:
&#39;e&#39; l&agrave; k&iacute; tự được th&ecirc;m v&agrave;o.</pre><br>*****<br><br>class Solution():<br>    def findTheDifference(self, s, t):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "abcd"
"abcde"
<br>     Expected Output:   "e"<br>Test case 2:<br>     Input:   "abcd"
"baacd"
<br>     Expected Output:   "a"<br>Test case 3:<br>     Input:   "rikkeirikkeirikkeirikkeirikkeirikkeirikkeirikkeirikkeirikkeirikkei"
"rikkeirikkeirikkeirikkeirikkeirikkeiirikkeirikkeirikkeirikkeirikkei"
<br>     Expected Output:   "i"<br>Test case 4:<br>     Input:   "findthedifference"
"findthediffference"
<br>     Expected Output:   "f"<br><br>*****