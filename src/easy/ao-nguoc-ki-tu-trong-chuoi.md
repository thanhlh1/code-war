<p>Đầu v&agrave;o l&agrave;&nbsp;một chuỗi, bạn cần đảo ngược thứ tự c&aacute;c k&yacute; tự trong mỗi từ trong chuỗi đầu v&agrave;o&nbsp;trong khi vẫn giữ nguy&ecirc;n khoảng trắng v&agrave; trật tự từ ban đầu.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> &quot;wellcome to global division of rikkeisoft&quot;
<strong>Đầu ra:</strong> &quot;emocllew ot labolg noisivid fo tfosiekkir&quot;</pre><br>*****<br><br>class Solution():<br>    def reverseWords(self, s):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "wellcome to global division of rikkeisoft"
<br>     Expected Output:   "emocllew ot labolg noisivid fo tfosiekkir"<br>Test case 2:<br>     Input:   "Let's take contest"
<br>     Expected Output:   "s'teL ekat tsetnoc"<br>Test case 3:<br>     Input:   "Rikkei !"
<br>     Expected Output:   "iekkiR !"<br>Test case 4:<br>     Input:   "Code Platform!"
<br>     Expected Output:   "edoC !mroftalP"<br><br>*****