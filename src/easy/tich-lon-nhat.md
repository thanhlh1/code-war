<p>&nbsp;</p>

<p>Cho một mảng số nguy&ecirc;n, t&igrave;m ba số c&oacute; t&iacute;ch l&agrave; lớn nhất&nbsp;v&agrave; đưa ra gi&aacute; trị lớn nhất&nbsp;đ&oacute;.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [1,2,3]
<strong>Đầu ra:</strong> 6
</pre>

<p>&nbsp;</p>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [1,2,3,4]
<strong>Đầu ra:</strong> 24</pre><br>*****<br><br>class Solution():<br>    def maximumProduct(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3]
<br>     Expected Output:   6<br>Test case 2:<br>     Input:   [1,2,3,4]
<br>     Expected Output:   24<br>Test case 3:<br>     Input:   [2,3,0]
<br>     Expected Output:   0<br>Test case 4:<br>     Input:   [-100,-11,-1,-3,-5,-24,-6,-9]
<br>     Expected Output:   -15<br><br>*****