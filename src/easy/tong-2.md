<p>C&oacute; một robot bắt đầu từ vị tr&iacute; (0, 0), gốc, tr&ecirc;n mặt phẳng 2D. Đưa ra một chuỗi c&aacute;c bước di chuyển của n&oacute;, ph&aacute;n đo&aacute;n nếu robot n&agrave;y kết th&uacute;c ở (0, 0) sau khi n&oacute; ho&agrave;n th&agrave;nh c&aacute;c bước di chuyển của n&oacute;.</p>

<p>Chuỗi di chuyển được biểu thị bằng một chuỗi v&agrave; k&yacute; tự di chuyển [i] thể hiện bước di chuyển thứ i của n&oacute;. Di chuyển hợp lệ l&agrave; R (phải), L (tr&aacute;i), U (l&ecirc;n) v&agrave; D (xuống). Nếu robot trở về nguồn gốc sau khi ho&agrave;n th&agrave;nh tất cả c&aacute;c bước di chuyển của n&oacute;, h&atilde;y trả về đ&uacute;ng. Nếu kh&ocirc;ng, trả lại sai.</p>

<p>Lưu &yacute;: C&aacute;ch m&agrave; robot &quot;facing&quot; l&agrave; kh&ocirc;ng li&ecirc;n quan. &quot;R&quot; sẽ lu&ocirc;n khiến robot di chuyển sang phải một lần, &quot;L&quot; sẽ lu&ocirc;n khiến n&oacute; di chuyển sang tr&aacute;i, v.v. Ngo&agrave;i ra, giả sử rằng độ lớn của chuyển động của robot l&agrave; giống nhau cho mỗi lần di chuyển.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> &quot;UD&quot;
<strong>Đầu ra:</strong> true 
<strong>Giải th&iacute;ch</strong>: Robot di chuyển l&ecirc;n một lần, rồi xuống một lần. Tất cả c&aacute;c di chuyển c&oacute; c&ugrave;ng độ lớn, v&igrave; vậy n&oacute; đ&atilde; kết th&uacute;c tại điểm gốc nơi n&oacute; bắt đầu.
</pre>

<p>&nbsp;</p>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> &quot;LL&quot;
<strong>Đầu ra:</strong> false
<strong>Giải th&iacute;ch</strong>: Robot di chuyển tr&aacute;i hai lần. N&oacute; kết th&uacute;c hai di chuyển b&ecirc;n tr&aacute;i của gốc xuất ph&aacute;t. V&igrave; vậy trả về false.</pre><br>*****<br><br>class Solution():<br>    def di_de_tro_ve(self, moves):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "UD"
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   "LL"
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   "UDDDURLLDLDU"
<br>     Expected Output:   false<br>Test case 4:<br>     Input:   "RLDU"
<br>     Expected Output:   true<br><br>*****