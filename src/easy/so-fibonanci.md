<p>C&aacute;c số Fibonacci, thường được k&yacute; hiệu l&agrave; F (n) tạo th&agrave;nh một chuỗi, được gọi l&agrave; chuỗi Fibonacci, sao cho mỗi số l&agrave; tổng của hai số trước, bắt đầu từ 0 v&agrave; 1.</p>

<p>Nghĩa l&agrave;: F (0) = 0, F (1) = 1</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;F (N) = F (N - 1) + F (N - 2), với N&gt; 1.</p>

<p>Cho N, t&iacute;nh F (N).</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> 2
<strong>Đầu ra:</strong> 1
<strong>Giải th&iacute;ch:</strong> F(2) = F(1) + F(0) = 1 + 0 = 1.
</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> 3
<strong>Đầu ra:</strong> 2
<strong>Giải th&iacute;ch:</strong> F(3) = F(2) + F(1) = 1 + 1 = 2.</pre><br>*****<br><br>class Solution():<br>    def fib(self, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   2
<br>     Expected Output:   1<br>Test case 2:<br>     Input:   3
<br>     Expected Output:   2<br>Test case 3:<br>     Input:   4
<br>     Expected Output:   3<br>Test case 4:<br>     Input:   0
<br>     Expected Output:   0<br><br>*****