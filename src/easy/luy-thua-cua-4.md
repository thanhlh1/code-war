<p>Cho 1 số kiểu integer (32 bit).</p>

<p>Viết một function để check xem số đ&oacute; c&oacute; l&agrave; kết quả của 4^n hay kh&ocirc;ng?</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>16
<strong>Đầu ra: </strong>true
</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>5
<strong>Đầu ra: </strong>false</pre><br>*****<br><br>class Solution():<br>    def isPowerOfFour(self, num):<br><br><br>*****<br><br>Test case 1:<br>     Input:   16
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   5
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   1
<br>     Expected Output:   true<br>Test case 4:<br>     Input:   0
<br>     Expected Output:   false<br><br>*****