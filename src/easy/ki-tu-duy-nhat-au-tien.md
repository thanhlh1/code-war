<p>Cho một string, t&igrave;m k&yacute; tự kh&ocirc;ng bị lặp lại tr&ecirc;n string đ&oacute; v&agrave; trả về index của k&yacute; tự đ&oacute;. Nếu kh&ocirc;ng tồn tại th&igrave; return -1, nếu tồn tại nhiều th&igrave; trả về index của k&yacute; tự đầu ti&ecirc;n.</p>

<p><strong>V&iacute; dụ:</strong></p>

<pre>
s = &quot;riikei&quot;
return 0.

s = &quot;s2rikkeis2&quot;,
return 2.</pre><br>*****<br><br>class Solution():<br>    def firstUniq(self, s):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "aadadaad"
<br>     Expected Output:   -1<br>Test case 2:<br>     Input:   "abcdef"
<br>     Expected Output:   0<br>Test case 3:<br>     Input:   "rikkei"
<br>     Expected Output:   0<br><br>*****