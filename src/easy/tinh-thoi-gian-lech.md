<p>Đưa ra danh s&aacute;ch c&aacute;c điểm thời gian trong 24 giờ ở định dạng &quot;Giờ:Ph&uacute;t&quot;, t&igrave;m ch&ecirc;nh lệch số ph&uacute;t tối thiểu giữa hai thời điểm bất kỳ trong danh s&aacute;ch.<br />
<strong>V&iacute; dụ:</strong><br />
Input:&nbsp; [&quot;23:59&quot;,&quot;0:07&quot;,&quot;0:02&quot;]<br />
Output:&nbsp; 3<br />
<strong>Ch&uacute; th&iacute;ch:</strong><br />
Số lượng điểm thời gian trong danh s&aacute;ch đ&atilde; cho &iacute;t nhất l&agrave; 2 v&agrave; sẽ kh&ocirc;ng vượt qu&aacute; 20000.<br />
Thời gian đầu v&agrave;o l&agrave; hợp lệ v&agrave; dao động từ 00:00 đến 23:59.</p><br>*****<br><br>class Solution():<br>    def tinh_thoi_gian_lech(self, timePs):<br><br><br>*****<br><br>Test case 1:<br>     Input:   ["23:59","0:07","0:02"]
<br>     Expected Output:   3<br>Test case 2:<br>     Input:   ["17:59","00:00"]
<br>     Expected Output:   361<br>Test case 3:<br>     Input:   ["00:00","00:00"]
<br>     Expected Output:   0<br>Test case 4:<br>     Input:   ["23:00","00:00"]
<br>     Expected Output:   60<br><br>*****