<p>Cho một chuỗi chỉ chứa c&aacute;c k&yacute; tự &#39;(&#39;, &#39;)&#39;, &#39;{&#39;, &#39;}&#39;, &#39;[&#39; v&agrave; &#39;]&#39;.</p>

<p>X&aacute;c định xem chuỗi nhập v&agrave;o c&oacute; hợp lệ hay kh&ocirc;ng.</p>

<p>Chuỗi đầu v&agrave;o hợp lệ nếu:</p>

<p>&nbsp; &nbsp; - Dấu ngoặc mở phải được đ&oacute;ng bởi c&ugrave;ng một loại dấu ngoặc.</p>

<p>&nbsp; &nbsp; - Dấu ngoặc mở phải được đ&oacute;ng theo đ&uacute;ng thứ tự.</p>

<p>&nbsp; &nbsp; - Ch&uacute; &yacute; rằng một chuỗi rỗng cũng được coi l&agrave; hợp lệ.</p>

<p><strong>V&iacute; dụ 1:</strong></p>

<pre>
<strong>    Input:</strong> &quot;()&quot;
<strong>    Output:</strong> true
</pre>

<p><strong>V&iacute; dụ 2:</strong></p>

<pre>
<strong>    Input:</strong> &quot;()[]{}&quot;
<strong>    Output:</strong> true
</pre>

<p><strong>V&iacute; dụ&nbsp;3:</strong></p>

<pre>
<strong>    Input:</strong> &quot;(]&quot;
<strong>    Output:</strong> false
</pre>

<p><strong>V&iacute; dụ&nbsp;4:</strong></p>

<pre>
<strong>    Input:</strong> &quot;([)]&quot;
<strong>    Output:</strong> false</pre><br>*****<br><br>class Solution():<br>    def isValid(self, s):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "(){}[]"
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   "()"
<br>     Expected Output:   true<br>Test case 3:<br>     Input:   ""
<br>     Expected Output:   true<br>Test case 4:<br>     Input:   "(]"
<br>     Expected Output:   false<br><br>*****