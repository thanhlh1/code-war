<p>Trong một mảng A c&oacute; k&iacute;ch thước 2N, c&oacute; c&aacute;c phần tử duy nhất N + 1 v&agrave; ch&iacute;nh x&aacute;c một trong c&aacute;c phần tử n&agrave;y được lặp lại N lần.</p>

<p>Trả về phần tử lặp lại N lần.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[1,2,3,3]
<strong>Đầu ra: </strong>3
</pre>

<p><strong>V&iacute; dụ 2:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[2,1,2,5,3,2]
<strong>Đầu ra: </strong>2</pre><br>*****<br><br>class Solution():<br>    def repeatedNTimes(self, A):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3,3]
<br>     Expected Output:   3<br>Test case 2:<br>     Input:   [2,1,2,5,3,2]
<br>     Expected Output:   2<br>Test case 3:<br>     Input:   [5,1,5,2,5,3,5,4]
<br>     Expected Output:   5<br>Test case 4:<br>     Input:   [1,2,3,1,1,1,1,5,7,9]
<br>     Expected Output:   1<br><br>*****