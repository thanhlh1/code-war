<p>&nbsp; &nbsp;Lo&agrave;i người ph&aacute;t hiện ra 1 ng&ocirc;n ngữ của một bộ tộc&nbsp;thổ d&acirc;n, đ&aacute;ng ngạc nhi&ecirc;n l&agrave; họ cũng sử dụng c&aacute;c chữ c&aacute;i viết thường bằng&nbsp;tiếng Anh, nhưng c&oacute; thể theo một thứ tự kh&aacute;c. Bảng chữ c&aacute;i của họ l&agrave; ho&aacute;n vị của bảng chữ c&aacute;i tiếng anh.</p>

<p>&nbsp; &nbsp; Cho một chuỗi order l&agrave; bảng chữ c&aacute;i của bộ tộc&nbsp;thổ d&acirc;n&nbsp;(bảng chữ c&aacute;i tiếng anh nhưng theo một thứ tự bất k&igrave;) v&agrave; một mảng words l&agrave; một mảng c&aacute;c chuỗi..</p>

<p>&nbsp; &nbsp; Nhiệm vụ của bạn l&agrave; kiểm tra xem mảng words&nbsp;đầu v&agrave;o c&oacute; được sắp xếp theo thứ tự từ nhỏ đến lớn theo thứ tự từ điển của bộ tộc đ&oacute;&nbsp;hay kh&ocirc;ng.</p>

<p>&nbsp;</p>

<p><strong>V&iacute; dụ 1:</strong></p>

<p>&nbsp; &nbsp; Đầu v&agrave;o: words = [&quot;rikkei&quot;, &quot;ricode&quot;, &quot;global&quot;], order = &quot;worldabcefghijkmnpqstuvxyz&quot;</p>

<p>&nbsp; &nbsp; Đầu ra: false&nbsp;</p>

<p>&nbsp; &nbsp; Giải th&iacute;ch: V&igrave; &#39;k&#39; xuất hiện sau &#39;c&#39; trong từ điển của bộ tộc thổ d&acirc;n&nbsp;v&igrave; thế n&ecirc;n&nbsp; rikkei&nbsp;&gt;&nbsp;ricode&nbsp;dẫn đến mảng words đầu v&agrave;o kh&ocirc;ng được sắp xếp từ nhỏ đến lớn.</p>

<p><strong>Th&ocirc;ng tin th&ecirc;m: </strong></p>

<p>&nbsp; &nbsp; 1 &lt;= words.length &lt;= 100 1 &lt;= từ [i] .length &lt;= 20 đặt h&agrave;ng.length == 26</p>

<p>&nbsp; &nbsp; Tất cả c&aacute;c k&yacute; tự trong từ [i] v&agrave; thứ tự l&agrave; chữ c&aacute;i viết thường tiếng Anh.</p><br>*****<br><br>class Solution():<br>    def ngon_ngu_tho_dan(self, words, order):<br><br><br>*****<br><br>Test case 1:<br>     Input:   ["rikkei", "ricode", "global"]
"worldabcefghijkmnpqstuvxyz"
<br>     Expected Output:   false<br>Test case 2:<br>     Input:   ["dark","walking"]
"worldabcefghijkmnpqstuvxyz"
<br>     Expected Output:   false<br>Test case 3:<br>     Input:    ["difference","example"]
"hlabcedfgijkmnopqrstuvwxyz"
<br>     Expected Output:   false<br>Test case 4:<br>     Input:   ["after","and"]
"hlabcedfgijkmnopqrstuvwxyz"
<br>     Expected Output:   true<br><br>*****