<p>Nam đi thi si&ecirc;u tr&iacute; tuệ, thử th&aacute;ch của Nam l&agrave;:<br />
Cho trước 1 chuỗi k&iacute; tự X, ban gi&aacute;m khảo sẽ thay đổi vị tr&iacute; c&aacute;c k&iacute; tự trong X v&agrave; th&ecirc;m v&agrave;o 2 đầu c&aacute;c k&iacute; tự bất k&igrave; tạo th&agrave;nh chuỗi Y.<br />
Nam phải t&igrave;m xem trong ma trận c&aacute;c chuỗi Y cho trước, chuỗi n&agrave;o được biến đổi từ X.<br />
Đồng h&agrave;nh c&ugrave;ng Nam l&agrave; robot Rikkei, bạn h&atilde;y viết chương tr&igrave;nh gi&uacute;p Rikkei chiến thắng Nam.</p>

<p><strong>Y&ecirc;u cầu:&nbsp;</strong><br />
<strong>Input</strong>: String X, Y<br />
<strong>Output:</strong> true, false.&nbsp;<br />
&nbsp;&nbsp; &nbsp;True nếu chuỗi Y được tạo th&agrave;nh từ chuỗi X<br />
&nbsp;&nbsp; &nbsp;False nếu ngược lại</p>

<p><strong>V&iacute; dụ 1:</strong><br />
Input: X = &quot;rik&quot;, Y = &quot;xxkriaaa&quot;<br />
Output: true<br />
Giải th&iacute;ch: &quot;kri&quot; l&agrave; &quot;rik&quot; sau khi đổi vị tr&iacute;</p>

<p><strong>V&iacute; dụ 2:</strong><br />
Input: X = &quot;rik&quot;, Y = &quot;xxkrai&quot;<br />
Output: false<br />
Giải th&iacute;ch: Y kh&ocirc;ng phải chuỗi tạo th&agrave;nh từ X. (&quot;krai&quot;)</p>

<p><strong>Note:</strong></p>

<p>LVL I: X.length, Y.length &lt; 20<br />
LVL II:&nbsp;X.length, Y.length &lt; 1000<br />
LVL III: X.length, Y.length &lt; 10000</p><br>*****<br><br>class Solution():<br>    def nam_di_thi_i(self, X, Y):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "rik"
"xxkriaa"
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   "rik"
"xxkrai"
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   "rikkei"
"adfafasfkekrii"
<br>     Expected Output:   true<br>Test case 4:<br>     Input:   "kkkeir"
"kkeirikkek"
<br>     Expected Output:   true<br><br>*****