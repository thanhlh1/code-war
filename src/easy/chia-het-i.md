<p>Nam v&agrave; Tiệp chơi 1 tr&ograve; chơi: mỗi người chọn 1 con số A v&agrave; B, ai liệt k&ecirc; được nhiều số chia hết cho A hoặc B l&agrave; người chiến thắng.<br />
H&atilde;y gi&uacute;p Nam chiến thắng Tiệp bằng c&aacute;ch viết 1 h&agrave;m trả về số thứ N chia hết cho A hoặc B.</p>

<p><strong>Input:</strong> N, A, B<br />
<strong>Output:</strong> số thứ N chia hết cho A hoặc B<br />
Kết quả c&oacute; thể rất lớn, trả về kết quả sau khi&nbsp;% (10^9 + 7)</p>

<p><strong>V&iacute; dụ 1:</strong>&nbsp;<br />
Input: 1&nbsp;2 3<br />
Output: 2<br />
Giải th&iacute;ch: c&aacute;c số chia hết c&oacute; 2 hoặc 3: 2,3,4,6,8,9</p>

<p><strong>V&iacute; dụ 2:</strong>&nbsp;<br />
Input: 7 5 6<br />
Output: 20<br />
Giải th&iacute;ch : C&aacute;c số chia hết cho 5 hoặc 6: 5,6,10,12,15,18,20,24,25</p>

<p><strong>Note:</strong> LVL I: n , a, b &lt; 100<br />
LVL II: n, a, b &lt; 10^ 9<br />
LVL III: n, a, b &lt; 10^9, time &lt; 0,1s</p><br>*****<br><br>class Solution():<br>    def chia_het_i(self, n, a, b):<br><br><br>*****<br><br>Test case 1:<br>     Input:   1
2
3
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   2
3
4
<br>     Expected Output:   4<br>Test case 3:<br>     Input:   4
6
20
<br>     Expected Output:   20<br><br>*****