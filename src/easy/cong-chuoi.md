<p>Cho hai số nguy&ecirc;n kh&ocirc;ng &acirc;m num1 v&agrave; num2 được biểu diễn dưới dạng chuỗi, trả về tổng của num1 v&agrave; num2.</p>

<p>Kết quả trả về l&agrave; một chuỗi biểu diễn tổng hai số đầu v&agrave;o.</p><br>*****<br><br>class Solution():<br>    def addStrings(self, num1, num2):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "1"
"2"
<br>     Expected Output:   "3"<br>Test case 2:<br>     Input:   "0"
"0"
<br>     Expected Output:   "0"<br>Test case 3:<br>     Input:   "51001234567890"
"51001234567890"
<br>     Expected Output:   "102002469135780"<br><br>*****