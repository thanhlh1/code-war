<p>Cho hai c&acirc;y nhị ph&acirc;n, viết một h&agrave;m để kiểm tra xem ch&uacute;ng c&oacute; giống nhau hay kh&ocirc;ng. Hai c&acirc;y nhị ph&acirc;n được coi l&agrave; giống nhau nếu ch&uacute;ng giống nhau về cấu tr&uacute;c v&agrave; c&aacute;c n&uacute;t c&oacute; c&ugrave;ng gi&aacute; trị.</p>

<p>V&iacute; dụ 1: Đầu v&agrave;o:&nbsp; &nbsp; 1&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;/&nbsp; &nbsp; &nbsp;\&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;/&nbsp; &nbsp; &nbsp;\ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 3&nbsp; &nbsp; 2&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p>[1,2,3], [1,2,3]</p>

<p>Đầu ra: đ&uacute;ng</p>

<p>V&iacute; dụ 2: Đầu v&agrave;o:&nbsp; 1&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;1&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;/&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p>[1,2], [1, null, 2]</p>

<p>Đầu ra: sai</p>

<p>&nbsp;</p><br>*****<br><br># class TreeNode:<br>#    def __init__(self, x):<br>#        self.val = x<br>#        self.left = None<br>#        self.right = None<br># define TreeNode use: lib.Library.TreeNode().<br>class Solution():<br>    def issametree(self, p, q):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3]
[1,2,3]
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   [1,2]
[1,null,2]
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   [1,2,1]
[1,1,2]
<br>     Expected Output:   false<br>Test case 4:<br>     Input:   [0,null]
[0,null]
<br>     Expected Output:   true<br><br>*****