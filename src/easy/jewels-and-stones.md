<p><code>X&acirc;u&nbsp;J</code>&nbsp;đại diện loại đ&aacute; l&agrave; đ&aacute; qu&yacute;, c&ograve;n&nbsp;<code>S</code>&nbsp;thể hiện số đ&aacute; bạn c&oacute;.&nbsp; Mỗi k&yacute; tự trong&nbsp;<code>S</code>&nbsp;l&agrave; một loại đ&aacute; bạn c&oacute;.&nbsp; Bạn muốn biết bao nhi&ecirc;u vi&ecirc;n đ&aacute; bạn sở hữu l&agrave; đ&aacute; qu&yacute;.</p>

<p>C&aacute;c k&yacute; tự trong&nbsp;<code>J</code>&nbsp;lu&ocirc;n ph&acirc;n biệt, v&agrave; mọi k&yacute; tự trong&nbsp;<code>J</code>&nbsp;v&agrave;&nbsp;<code>S</code>&nbsp;đều l&agrave; chữ c&aacute;i. C&aacute;c chữ c&aacute;i c&oacute; ph&acirc;n biệt chữ hoa v&agrave; chữ thường,&nbsp;<code>&quot;a&quot;</code>&nbsp;l&agrave; một loại đ&aacute; kh&aacute;c với&nbsp;<code>&quot;A&quot;</code>.</p>

<p><strong>V&iacute; dụ 1:</strong></p>

<pre>
<strong>Input:</strong> J = &quot;aA&quot;, S = &quot;aAAbbbb&quot;
<strong>Output:</strong> 3
</pre>

<p><strong>V&iacute; dụ 2:</strong></p>

<pre>
<strong>Input:</strong> J = &quot;z&quot;, S = &quot;ZZ&quot;
<strong>Output:</strong> 0</pre><br>*****<br><br>class Solution():<br>    def jewels_and_stones(self, j, s):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "aA"
"aAAbbbb"
<br>     Expected Output:   3<br>Test case 2:<br>     Input:   "z"
"ZZ"
<br>     Expected Output:   0<br>Test case 3:<br>     Input:   "abc"
"aAAbbdfgdgfbb"
<br>     Expected Output:   5<br>Test case 4:<br>     Input:   "sdf"
"aAAbgdfbbb"
<br>     Expected Output:   2<br><br>*****