<p>T&igrave;m phần tử lớn thứ k trong một mảng chưa sắp xếp.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> <code>[3,2,1,5,6,4] </code>v&agrave; k = 2
<strong>Đầu ra:</strong> 5
</pre>

<p><strong>V&iacute; dụ 2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> <code>[3,2,3,1,2,4,5,5,6] </code>v&agrave; k = 4
<strong>Đầu ra:</strong> 4</pre><br>*****<br><br>class Solution():<br>    def findKthLargest(self, nums, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [3,2,1,5,6,4]
2
<br>     Expected Output:   5<br><br>*****