<p>Cho một số nguy&ecirc;n, viết một h&agrave;m để x&aacute;c định xem đ&oacute; c&oacute; phải l&agrave; lũy thừa của 3 kh&ocirc;ng.</p><br>*****<br><br>class Solution():<br>    def isPowerOfThree(self, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   27
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   0
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   9
<br>     Expected Output:   true<br>Test case 4:<br>     Input:   45
<br>     Expected Output:   false<br><br>*****