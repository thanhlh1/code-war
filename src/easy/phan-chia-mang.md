<p>Cho một mảng gồm 2n số nguy&ecirc;n, nhiệm vụ của bạn l&agrave; nh&oacute;m c&aacute;c số nguy&ecirc;n n&agrave;y th&agrave;nh n cặp số nguy&ecirc;n.</p>

<p>Giả sử (a1, b1), (a2, b2), ..., (an, bn) tạo th&agrave;nh tổng của min (ai, bi) cho tất cả i từ 1 đến n c&agrave;ng lớn c&agrave;ng tốt.</p>

<p>Đưa ra tổng đ&oacute;.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [1,4,3,2]

<strong>Đầu ra:</strong> 4
<strong>Giải th&iacute;ch:</strong> n l&agrave; 2, tổng lớn nhất của c&aacute;c phần l&agrave;: min(1, 2) + min(3, 4) = 1 + 3 = 4.</pre><br>*****<br><br>class Solution():<br>    def arrayPair(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,4,3,2]
<br>     Expected Output:   4<br>Test case 2:<br>     Input:   [0,1234]
<br>     Expected Output:   0<br>Test case 3:<br>     Input:   [-12,22,1,3]
<br>     Expected Output:   -9<br>Test case 4:<br>     Input:   [10000,10000,10000,10000,10000,10000,10000,10000,10000,1000]
<br>     Expected Output:   41000<br><br>*****