<p>&nbsp;</p>

<p>Cho&nbsp;một mảng c&aacute;c số nguy&ecirc;n kh&ocirc;ng &acirc;m, ban đầu bạn được định vị ở chỉ mục đầu ti&ecirc;n của mảng.</p>

<p>Mỗi phần tử trong mảng biểu thị độ d&agrave;i bước nhảy tối đa của bạn tại vị tr&iacute; đ&oacute;.</p>

<p>X&aacute;c định xem bạn c&oacute; thể đạt được chỉ số cuối c&ugrave;ng kh&ocirc;ng.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [2,3,1,1,4]
<strong>Đầu ra:</strong> true
<strong>Giải th&iacute;ch:</strong> Nhảy 1 bước từ chỉ số 0 l&ecirc;n 1, sau đ&oacute; 3 bước đến chỉ mục cuối c&ugrave;ng.
</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [3,2,1,0,4]
<strong>Đầu ra:</strong> false
<strong>Giải th&iacute;ch:</strong> Bạn sẽ lu&ocirc;n đến chỉ số 3 bất kể điều g&igrave;. Độ d&agrave;i bước nhảy l&agrave; 0, khiến n&oacute; kh&ocirc;ng thể đạt được chỉ số cuối c&ugrave;ng.</pre><br>*****<br><br>class Solution():<br>    def perfectJump(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [0,100,4,5,6,7,0,5]
<br>     Expected Output:   false<br>Test case 2:<br>     Input:   [0,43,5,63,6,7,0,5]
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   [1,3,5, 5,0,0,0,0,0,5,0,7,0,5]
<br>     Expected Output:   false<br>Test case 4:<br>     Input:   [3,9,0,0,0,0,0,0,0,10]
<br>     Expected Output:   true<br><br>*****