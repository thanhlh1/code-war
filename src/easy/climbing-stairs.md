<p>Bạn đang leo l&ecirc;n một&nbsp;cầu thang. Phải mất n bước để đạt đến đỉnh.</p>

<p>Mỗi lần bạn c&oacute; thể leo 1 hoặc 2 bước. C&oacute;&nbsp;bao nhi&ecirc;u c&aacute;ch kh&aacute;c nhau bạn c&oacute; thể leo l&ecirc;n đỉnh?</p>

<p><strong>V&iacute; dụ 1:</strong></p>

<pre>
<strong>Input:</strong> 2
<strong>Output:</strong> 2</pre>

<p><strong>V&iacute; dụ 2:</strong></p>

<pre>
<strong>Input:</strong> 3
<strong>Output:</strong> 3
</pre><br>*****<br><br>class Solution():<br>    def climbing_stairs(self, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   2
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   3
<br>     Expected Output:   3<br>Test case 3:<br>     Input:   6
<br>     Expected Output:   13<br>Test case 4:<br>     Input:   24
<br>     Expected Output:   75025<br><br>*****