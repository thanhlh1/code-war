<p>Ch&uacute;ng t&ocirc;i c&oacute; một bộ sưu tập đ&aacute;, mỗi tảng đ&aacute; c&oacute; trọng số nguy&ecirc;n dương được biểu diễn bởi mảng stones.</p>

<p>Mỗi lượt, ch&uacute;ng t&ocirc;i chọn hai tảng đ&aacute; nặng nhất v&agrave; đập ch&uacute;ng lại với nhau.</p>

<p>Giả sử c&aacute;c vi&ecirc;n đ&aacute; c&oacute; trọng số x v&agrave; y với x &lt;= y.</p>

<p>Kết quả của sự cố n&agrave;y l&agrave;:</p>

<p>&nbsp; &nbsp; Nếu x == y, cả hai vi&ecirc;n đ&aacute; đều bị ph&aacute; hủy ho&agrave;n to&agrave;n;</p>

<p>&nbsp; &nbsp; Nếu x! = Y, vi&ecirc;n đ&aacute; c&oacute; trọng lượng x bị ph&aacute; hủy ho&agrave;n to&agrave;n v&agrave; vi&ecirc;n đ&aacute; c&oacute; trọng lượng y c&oacute; trọng lượng mới y-x.</p>

<p>Cuối c&ugrave;ng, c&oacute; nhiều nhất 1 vi&ecirc;n đ&aacute; c&ograve;n lại. Trả lại trọng lượng của vi&ecirc;n đ&aacute; n&agrave;y (hoặc 0 nếu kh&ocirc;ng c&ograve;n vi&ecirc;n đ&aacute; n&agrave;o.</p>

<p>V&iacute; dụ<strong> 1:</strong></p>

<pre>
<strong>Đầu v&agrave;o</strong><strong>: </strong>[2,7,4,1,8,1]
<strong>Đầu ra: </strong>1
<strong>Giải th&iacute;ch: </strong>
Đập 7 v&agrave; 8 để c&oacute; 1, mảng mới l&agrave; [2,4,1,1,1]
Đập 2 v&agrave; 4 để c&oacute; được 2, mảng mới l&agrave; [2,1,1,1]
Đập 2 v&agrave; 1 để c&oacute; 1, mảng mới l&agrave; [1,1,1]
Đập 1 v&agrave; 1 để nhận 0, mảng mới l&agrave; [1] th&igrave; đ&oacute; l&agrave; gi&aacute; trị của vi&ecirc;n đ&aacute; cuối c&ugrave;ng.</pre><br>*****<br><br>class Solution():<br>    def lastStone(self, stones):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [2,7,4,1,8,1]
<br>     Expected Output:   1<br>Test case 2:<br>     Input:   [3,100,23,400,23,19,43,31,491,489,333,231,5,33]
<br>     Expected Output:   0<br>Test case 3:<br>     Input:   [3,100,23,40,333,231,5,33]
<br>     Expected Output:   6<br>Test case 4:<br>     Input:   [31,35,214,251,51,454,663,33,1,35,7,3,562,672,22,11,62]
<br>     Expected Output:   1<br><br>*****