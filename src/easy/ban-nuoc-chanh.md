<p>Tại một quầy b&aacute;n nước chanh, mỗi ly nước chanh c&oacute; gi&aacute; $5.</p>

<p>&nbsp;</p>

<p>Kh&aacute;ch h&agrave;ng đang đứng xếp h&agrave;ng để mua h&agrave;ng của bạn v&agrave; đặt h&agrave;ng tại một thời điểm (theo thứ tự được chỉ định bởi c&aacute;c h&oacute;a đơn).</p>

<p>Mỗi kh&aacute;ch h&agrave;ng chỉ mua 1 li nước chanh v&agrave; thanh to&aacute;n bằng 3 mệnh gi&aacute; l&agrave; 5$, 10$, 20$. Số tiền m&agrave; mỗi người chuẩn bị được biểu diễn bởi 1 mảng số nguy&ecirc;n l&agrave; bills. Bạn cần trả lại tiền thừa cho từng kh&aacute;ch.</p>

<p>Ban đầu, bạn kh&ocirc;ng c&oacute; đồng n&agrave;o trong tay.</p>

<p>Kiểm tra xem bạn c&oacute; thể ho&agrave;n th&agrave;nh tất cả c&aacute;c giao dịch được hay kh&ocirc;ng.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[5,5,5,10,20]
<strong>Đầu ra: </strong>true
<strong>Giải th&iacute;ch: </strong>
Giao dịch xong với người thứ 3, ta c&oacute; 3 tờ 5$ trong t&uacute;i..
Đối với người thứ 4, ta lấy về 10$ v&agrave; trả lại 5$.
Đối với người thứ 5, ta trả lại 1 tờ 10$ v&agrave; 1 tờ 5$.
Tất cả giao dịch được ho&agrave;n tất, ta trả về true.
</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[5,5,10]
<strong>Đầu ra: </strong>true
</pre>

<p><strong>V&iacute; dụ&nbsp;3:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[10,10]
<strong>Đầu ra: </strong>false</pre>

<p>&nbsp;</p>

<p>&nbsp;</p><br>*****<br><br>class Solution():<br>    def ban_nuoc_chanh(self, bills):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [5,5,5,10,20]
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   [5,5,10]
<br>     Expected Output:   true<br>Test case 3:<br>     Input:   [10,10]
<br>     Expected Output:   false<br>Test case 4:<br>     Input:   [5,5,10,10,20]
<br>     Expected Output:   false<br><br>*****