<p>Cho một mảng&nbsp;c&aacute;c số nguy&ecirc;n kh&ocirc;ng &acirc;m, sắp xếp ch&uacute;ng sao cho ch&uacute;ng tạo th&agrave;nh số lớn nhất.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> <code>[10,2]</code>
<strong>Đầu ra:</strong> &quot;<code>210&quot;</code></pre>

<p><strong>V&iacute; dụ 2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> <code>[3,30,34,5,9]</code>
<strong>Đầu ra:</strong> &quot;<code>9534330&quot;</code></pre><br>*****<br><br>class Solution():<br>    def largestNumber(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [10,2]
<br>     Expected Output:   "210"<br><br>*****