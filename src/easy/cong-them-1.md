<p>Cho một mảng kh&ocirc;ng rỗng để biểu thị một số nguy&ecirc;n kh&ocirc;ng &acirc;m. Cộng th&ecirc;m 1 đơn vị v&agrave;o số nguy&ecirc;n được biểu thị.</p>

<p>C&aacute;c chữ số được luu v&agrave;o mảng sao cho số c&oacute; &yacute; nghĩ nhất nằm ở đầu, mỗi phần tử mảng lưu một chữ số.</p>

<p>Ngầm hiểu: Phần tử đầu của mảng đầu ra&nbsp;lu&ocirc;n kh&aacute;c 0 (Kh&ocirc;ng c&oacute; số nguy&ecirc;n dương n&agrave;o cộng 1 bằng 0).</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o :</strong> [1,2,3]
<strong>Đầu ra  :</strong> [1,2,4]
<strong>Giải th&iacute;ch:</strong> Mảng đầu v&agrave;o biểu thị cho số nguy&ecirc;n 123.
</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [4,3,2,9]
<strong>Đầu ra :</strong> [4,3,3,0]
<strong>Giải th&iacute;ch:</strong> Mảng đầu v&agrave;o biểu thị cho số nguy&ecirc;n 4329.</pre><br>*****<br><br>class Solution():<br>    def plus(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3]
<br>     Expected Output:   [1,2,4]<br>Test case 2:<br>     Input:   [4,3,2,2]
<br>     Expected Output:   [4,3,2,3]<br>Test case 3:<br>     Input:   [9,9,9]
<br>     Expected Output:   [1,0,0,0]<br>Test case 4:<br>     Input:   [0]
<br>     Expected Output:   [1]<br><br>*****