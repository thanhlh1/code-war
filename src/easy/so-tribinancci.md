<p>Chuỗi Tribonacci được định nghĩa như sau:</p>

<p>T0 = 0, T1 = 1, T2 = 1 v&agrave; Tn + 3 = Tn + Tn + 1 + Tn + 2 với n&gt; = 0.</p>

<p>Cho n, trả về gi&aacute; trị của Tn.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> n = 4
<strong>Đầu ra:</strong> 4
<strong>Giải th&iacute;ch:</strong>
T_3 = 0 + 1 + 1 = 2
T_4 = 1 + 1 + 2 = 4</pre><br>*****<br><br>class Solution():<br>    def tribonacci(self, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   10
<br>     Expected Output:   149<br>Test case 2:<br>     Input:   4
<br>     Expected Output:   4<br>Test case 3:<br>     Input:   0
<br>     Expected Output:   0<br>Test case 4:<br>     Input:   22
<br>     Expected Output:   223317<br><br>*****