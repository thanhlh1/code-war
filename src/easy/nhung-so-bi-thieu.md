<p>Cho một mảng c&aacute;c số nguy&ecirc;n trong đ&oacute; 1 &le; a [i] &lt;= n (n = k&iacute;ch thước của mảng), một số phần tử xuất hiện hai lần v&agrave; c&aacute;c phần tử kh&aacute;c xuất hiện một lần.</p>

<p>T&igrave;m tất cả c&aacute;c yếu tố trong khoảng [1, n] m&agrave; kh&ocirc;ng xuất hiện trong mảng n&agrave;y.</p>

<p>Bạn c&oacute; thể l&agrave;m điều đ&oacute; m&agrave; kh&ocirc;ng cần th&ecirc;m kh&ocirc;ng gian v&agrave; trong thời gian chạy O (n) kh&ocirc;ng?</p>

<p>Bạn c&oacute; thể cho rằng danh s&aacute;ch trả về kh&ocirc;ng được t&iacute;nh l&agrave; kh&ocirc;ng gian th&ecirc;m.</p>

<p><strong>V&iacute; dụ:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong>
[4,3,2,7,8,2,3,1]

<strong>Đầu ra:</strong>
[5,6]</pre><br>*****<br><br>class Solution():<br>    def findNumbers(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [4,3,2,7,8,2,3,1]
<br>     Expected Output:   [5,6]<br>Test case 2:<br>     Input:   [1,2,3,5,7,8,5,2]
<br>     Expected Output:   [4,6]<br>Test case 3:<br>     Input:   [2,3,4,5,6,7,8,9,8,7,6,5,1,1]
<br>     Expected Output:   [10,11,12,13,14]<br><br>*****