<p>Th&uacute;y&nbsp;nu&ocirc;i n ch&uacute; thỏ bảy m&agrave;u trong chuồng, do ch&uacute;ng giao phối cận huyết n&ecirc;n con sinh ra c&oacute; rất nhiều m&agrave;u sắc.&nbsp;<br />
Tuy nhi&ecirc;n, Th&uacute;y&nbsp;chỉ giữ lại mỗi m&agrave;u 1 con.<br />
Một ng&agrave;y đẹp trời Th&uacute;y&nbsp;thăm chuồng thỏ th&igrave; thấy c&oacute; n con thỏ nhưng c&oacute; một số con bị tr&ugrave;ng m&agrave;u.<br />
Sau một hồi suy nghĩ, Th&uacute;y&nbsp;ngộ ra rằng cần phải loại bỏ một số con thỏ.&nbsp;</p>

<p>M&agrave;u của những con thỏ được đ&aacute;nh số bằng c&aacute;c số nguy&ecirc;n từ 1.<br />
Giả sử tất cả những con thỏ cần phải loại bỏ đều c&ugrave;ng 1 m&agrave;u.</p>

<p><br />
H&atilde;y gi&uacute;p Th&uacute;y&nbsp;t&igrave;m ra những con thỏ cần phải loại bỏ.</p>

<p>Input: mảng c&aacute;c số nguy&ecirc;n tượng trưng cho m&agrave;u của c&aacute;c con thỏ<br />
&nbsp;&nbsp; &nbsp;(Chỉ c&oacute; duy nhất 1 số bị lặp lại trong mảng)<br />
Output: 1 số l&agrave; m&agrave;u của những con thỏ bị loại</p>

<p>V&iacute; dụ:&nbsp;<br />
Input: [1,2,3,4,5,1]<br />
Output: 1</p>

<p>Input: [1,1,1,1,2,3]<br />
Output: 1</p>

<p>Input: [5,1,3,4,4,4]<br />
Output: 4</p><br>*****<br><br>class Solution():<br>    def loai_tru_tho(self, rabbit):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3,4,5,1]
<br>     Expected Output:   1<br>Test case 2:<br>     Input:   [1,1,1,1,2,3]
<br>     Expected Output:   1<br>Test case 3:<br>     Input:   [5,1,3,4,4,4]
<br>     Expected Output:   4<br>Test case 4:<br>     Input:   [4,4,4,4,1,2,3,4,4,4,4,4,4,4]
<br>     Expected Output:   4<br><br>*****