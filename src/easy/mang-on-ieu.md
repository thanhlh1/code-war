<p>&nbsp;</p>

<p>Một mảng l&agrave; đơn điệu nếu n&oacute; l&agrave; tăng đơn điệu hoặc giảm đơn điệu.</p>

<p>Một mảng A l&agrave; đơn điệu tăng nếu với tất cả i &lt;= j, A [i] &lt;= A [j].</p>

<p>Một mảng A l&agrave; đơn điệu giảm nếu với tất cả i &lt;= j, A [i]&gt; = A [j].</p>

<p>Trả về true khi v&agrave; chỉ khi mảng A đ&atilde; cho l&agrave; đơn điệu.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[1,2,2,3]
<strong>Đầu ra: </strong>true
</pre>

<p><strong>V&iacute; dụ&nbsp; 2:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[6,5,4,4]
<strong>Đầu ra: </strong>true
</pre>

<p><strong>V&iacute; dụ&nbsp; 3:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[1,3,2]
<strong>Đầu ra: </strong>false</pre><br>*****<br><br>class Solution():<br>    def isMonotonic(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,2,3]
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   [6,5,4,4]
<br>     Expected Output:   true<br>Test case 3:<br>     Input:   [1,3,2]
<br>     Expected Output:   false<br>Test case 4:<br>     Input:   [1,2,4,5]
<br>     Expected Output:   true<br><br>*****