<p>&nbsp; &nbsp; Bạn l&agrave; một c&ocirc; gi&aacute;o mầm non dễ thương&nbsp;v&agrave; bạn muốn ph&aacute;t b&aacute;nh quy cho c&aacute;c học sinh của m&igrave;nh. Bạn phải đảm bảo rằng mỗi đứa trẻ chỉ được ph&eacute;p nhận được tối đa một chiếc b&aacute;nh&nbsp;quy.</p>

<p>&nbsp; &nbsp; Loại b&aacute;nh quy n&agrave;y l&agrave; loại c&oacute; nhiều k&iacute;ch thước được thể hiện bằng số nguy&ecirc;n bởi mỗi phần tử trong mảng cookie.</p>

<p>&nbsp; &nbsp; Bọn trẻ kh&ocirc;ng muốn nhận được b&aacute;nh quy&nbsp;nhỏ hơn&nbsp;so với mong muốn của ch&uacute;ng. Mong muốn của bọn trẻ được thể hiện bằng 1 số nguy&ecirc;n v&agrave; l&agrave; gi&aacute; trị của một phần tử trong mảng child.</p>

<p>&nbsp; &nbsp; H&atilde;y cho biết c&oacute; tối đa bao nhi&ecirc;u đứa trẻ nhận được b&aacute;nh theo đ&uacute;ng mong muốn.&nbsp;&nbsp;</p>

<p><strong>Ch&uacute; th&iacute;ch:</strong><br />
&nbsp; &nbsp; Bạn kh&ocirc;ng thể ph&aacute;t&nbsp;nhiều hơn một chiếc b&aacute;nh&nbsp;cho một đứa trẻ.</p>

<p><strong>Đầu v&agrave;o:</strong></p>

<p>&nbsp; &nbsp; int[] child: mảng thể hiện c&aacute;c đứa trẻ v&agrave; child[i] thể hiện mong muốn của đứa trẻ i.</p>

<p>&nbsp; &nbsp;&nbsp;int[] cookie : mảng thể hiện số b&aacute;nh quy&nbsp;v&agrave; cookie[j] thể hiện k&iacute;ch thước của&nbsp;b&aacute;nh quy j.</p>

<p><strong>Đầu ra:</strong></p>

<p>&nbsp; &nbsp; Số nguy&ecirc;n thể hiện số đứa trẻ tối đa nhận được b&aacute;nh quy.</p>

<p><strong>V&iacute; dụ 1:</strong><br />
&nbsp; &nbsp; Đầu v&agrave;o: [1,2,3], [1,1]</p>

<p>&nbsp; &nbsp; Đầu ra: 1</p>

<p>&nbsp; &nbsp; Giải th&iacute;ch: Bạn c&oacute; 3 đứa trẻ&nbsp;v&agrave; 2 b&aacute;nh quy.</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; - Mong muốn của 3 đứa trẻ lần lượt l&agrave; 1, 2 v&agrave; 3</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; - K&iacute;ch thước 2 b&aacute;nh quy l&agrave; 1 v&agrave; 1</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; - D&ugrave; bạn c&oacute; 2 chiếc b&aacute;nh quy tuy nhi&ecirc;n k&iacute;ch thước của ch&uacute;ng l&agrave; 1 n&ecirc;n chỉ c&oacute; thể đ&aacute;p ứng cho 1 đứa trẻ. V&igrave; vậy, đầu ra sẽ l&agrave; 1.<br />
<strong>V&iacute; dụ 2</strong>:<br />
&nbsp; &nbsp; Đầu v&agrave;o: [1,2], [1,2,3]</p>

<p>&nbsp; &nbsp; Đầu ra: 2</p>

<p>&nbsp; &nbsp; Giải th&iacute;ch: Bạn c&oacute; 2 đứa trẻ&nbsp;v&agrave; 3 b&aacute;nh quy.</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; - Mong muốn của 2 đứa trẻ lần lượt l&agrave; 1 v&agrave;&nbsp;2&nbsp;</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; - K&iacute;ch thước 3 b&aacute;nh quy l&agrave; 1, 2&nbsp; v&agrave; 3</p>

<p>&nbsp; &nbsp; &nbsp; &nbsp; - Bạn c&oacute; đủ số b&aacute;nh quy để đ&aacute;p ứng 2 đứa trẻ n&ecirc;n đầu ra sẽ l&agrave; 2</p><br>*****<br><br>class Solution():<br>    def phat_banh_quy(self, child, cookie):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3]
[1,1]
<br>     Expected Output:   1<br>Test case 2:<br>     Input:   [1,2]
[1,2,3]
<br>     Expected Output:   2<br>Test case 3:<br>     Input:   [1,1]
[2]
<br>     Expected Output:   1<br>Test case 4:<br>     Input:   [1,5,6]
[10]
<br>     Expected Output:   1<br><br>*****