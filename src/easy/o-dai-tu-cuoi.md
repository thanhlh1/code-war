<p>Cho một chuỗi s bao gồm c&aacute;c chữ c&aacute;i in hoa / in thường v&agrave; c&aacute;c k&yacute; tự khoảng trống .&nbsp;</p>

<p>Trả về độ d&agrave;i của từ cuối c&ugrave;ng trong chuỗi.</p>

<p>Nếu từ cuối c&ugrave;ng kh&ocirc;ng tồn tại, trả về 0.</p>

<p>Lưu &yacute;: Một từ được định nghĩa l&agrave; một chuỗi k&yacute; tự chỉ bao gồm c&aacute;c k&yacute; tự kh&ocirc;ng phải khoảng trắng.</p>

<p>V&iacute; dụ :</p>

<pre>
<strong>    Input:</strong> &quot;Hello World&quot;
<strong>    Output:</strong> 5</pre><br>*****<br><br>class Solution():<br>    def lengthWord(self, s):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "Hello World"
<br>     Expected Output:   5<br>Test case 2:<br>     Input:   "Hello !"
<br>     Expected Output:   1<br>Test case 3:<br>     Input:   "Welcome "
<br>     Expected Output:   7<br>Test case 4:<br>     Input:   " "
<br>     Expected Output:   0<br><br>*****