<p>Cho một số nguy&ecirc;n kh&ocirc;ng &acirc;m c, nhiệm vụ của bạn l&agrave; kiểm tra&nbsp;c&oacute; tồn tại&nbsp;hai số nguy&ecirc;n a v&agrave; b sao cho a^2 + b^2 = c hay kh&ocirc;ng.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> 5
<strong>Đầu ra:</strong> true
<strong>Giải th&iacute;ch:</strong> 1 * 1 + 2 * 2 = 5
</pre>

<p>&nbsp;</p>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> 3
<strong>Đầu ra:</strong> False</pre><br>*****<br><br>class Solution():<br>    def SquareSum(self, c):<br><br><br>*****<br><br>Test case 1:<br>     Input:   5
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   3
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   1234567890
<br>     Expected Output:   false<br>Test case 4:<br>     Input:   125
<br>     Expected Output:   true<br><br>*****