<p>Nh&acirc;n vi&ecirc;n Rikkei vừa tham gia thi s&aacute;t hạch, điểm thi của mỗi người được lưu v&agrave;o 1 mảng<br />
Để tiện thống k&ecirc;, ban tổ chức muốn lọc ra những người c&oacute; số điểm gần nhau v&agrave;o 1 nh&oacute;m.&nbsp;<br />
Gọi X l&agrave; ch&ecirc;nh lệch điểm tối thiểu giữa 2 nh&acirc;n vi&ecirc;n bất k&igrave;.<br />
Cứ 2 người A,B c&oacute; số điểm lệch nhau X th&igrave; được cho v&agrave;o 1 nh&oacute;m, một người c&oacute; thể tham gia nhiều nh&oacute;m.<br />
Trả về mảng c&aacute;c cặp A,B đ&oacute; theo mẫu: Số nhỏ đứng trước, số lớn đứng sau.<br />
Input: int[]<br />
Output: int[][]&nbsp;</p>

<p>V&iacute; dụ 1:<br />
Đầu v&agrave;o: arr= [4,2,1,3]<br />
Đầu ra: [[1,2],[2,3],[3,4]]</p>

<p>Giải th&iacute;ch: Ch&ecirc;nh lệch tối thiểu l&agrave; 1. Liệt k&ecirc; tất cả c&aacute;c cặp c&oacute; ch&ecirc;nh lệch bằng 1 theo thứ tự tăng dần.</p>

<p>V&iacute; dụ 2:<br />
Đầu v&agrave;o: arr = [1,3,6,10,15]<br />
Đầu ra: [[1,3]]</p>

<p>V&iacute; dụ 3:<br />
Đầu v&agrave;o: arr = [3,8, -10,23,19, -4, -14,27]<br />
Đầu ra: [[-14, -10], [19,23], [23,27]]<br />
&nbsp;<br />
C&aacute;c r&agrave;ng buộc:<br />
2 &lt;= arr.length &lt;= 10^5<br />
-10^6 &lt;= arr[i] &lt;= 10^6</p><br>*****<br><br>class Solution():<br>    def caothap(self, arr):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [4,2,1,3]
<br>     Expected Output:   [[1,2],[2,3],[3,4]]<br>Test case 2:<br>     Input:   [1,3,6,10,15]
<br>     Expected Output:   [[1,3]]<br>Test case 3:<br>     Input:   [3,8, -10,23,19, -4, -14,27]
<br>     Expected Output:   [[-14,-10],[19,23],[23,27]]<br>Test case 4:<br>     Input:   [0,1]
<br>     Expected Output:   [[0,1]]<br><br>*****