<p>Cho một mảng c&aacute;c số nguy&ecirc;n, t&igrave;m xem mảng c&oacute; chứa bất kỳ bản sao n&agrave;o kh&ocirc;ng. H&agrave;m của bạn sẽ trả về true nếu bất kỳ gi&aacute; trị n&agrave;o xuất hiện &iacute;t nhất hai lần trong mảng v&agrave; n&oacute; sẽ trả về false nếu mọi phần tử đều kh&aacute;c biệt</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> [1,2,3,1]
<strong>Đầu ra:</strong> true</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[1,2,3,4]
<strong>Đầu ra:</strong> false</pre><br>*****<br><br>class Solution():<br>    def containsDuplicate(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3,1]
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   [1,2,3,4]
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   [1,1,1,3,3,4,3,2,4,2]
<br>     Expected Output:   true<br>Test case 4:<br>     Input:   [1]
<br>     Expected Output:   false<br><br>*****