<p>Viết chương tr&igrave;nh để kiểm tra xem một số đ&atilde; cho c&oacute; phải l&agrave; số xấu kh&ocirc;ng.</p>

<p>Số xấu l&agrave; số dương c&oacute; c&aacute;c thừa số nguy&ecirc;n tố chỉ bao gồm 2, 3, 5.</p>

<p>Số 1 thường được coi l&agrave; một con số xấu x&iacute;.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> 6
<strong>Đầu ra:</strong> true
<strong>Giải th&iacute;ch: </strong>6 = 2 &times;&nbsp;3</pre>

<p><strong>V&iacute; dụ&nbsp;3:</strong></p>

<pre>
<strong>Đầu v&agrave;o:</strong> 14
<strong>Đầu ra:</strong> false 
<strong>Giải th&iacute;ch: </strong><code>14</code> kh&ocirc;ng phải l&agrave; một con số xấu x&iacute; bởi n&oacute; c&oacute; thừa số nguy&ecirc;n tố l&agrave; <code>7</code>.</pre><br>*****<br><br>class Solution():<br>    def isUgly(self, num):<br><br><br>*****<br><br>Test case 1:<br>     Input:   6
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   8
<br>     Expected Output:   true<br>Test case 3:<br>     Input:   1
<br>     Expected Output:   true<br>Test case 4:<br>     Input:   14
<br>     Expected Output:   false<br><br>*****