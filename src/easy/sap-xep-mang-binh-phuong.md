<p>&nbsp;</p>

<p>Cho một mảng c&aacute;c số nguy&ecirc;n A được sắp xếp theo thứ tự kh&ocirc;ng giảm, trả về một mảng c&aacute;c b&igrave;nh phương của mỗi số, cũng theo thứ tự kh&ocirc;ng giảm được sắp xếp.</p>

<p><strong>V&iacute; dụ&nbsp;1:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[-4,-1,0,3,10]
<strong>Đầu ra: </strong>[0,1,9,16,100]
</pre>

<p><strong>V&iacute; dụ&nbsp;2:</strong></p>

<pre>
<strong>Đầu v&agrave;o: </strong>[-7,-3,2,3,11]
<strong>Đầu ra: </strong>[4,9,9,49,121]</pre><br>*****<br><br>class Solution():<br>    def sortedSquares(self, arr):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [-4,-1,0,3,10]
<br>     Expected Output:   [0,1,9,16,100]<br>Test case 2:<br>     Input:   [-7,-3,2,3,11]
<br>     Expected Output:   [4,9,9,49,121]<br>Test case 3:<br>     Input:   [-32,-21,-4,10,29,31,419,1490]
<br>     Expected Output:   [16,100,441,841,961,1024,175561,2220100]<br>Test case 4:<br>     Input:   [-100,24,254,300,1412]
<br>     Expected Output:   [576,10000,64516,90000,1993744]<br><br>*****