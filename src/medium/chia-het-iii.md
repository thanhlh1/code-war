<p>Nam v&agrave; Tiệp chơi 1 tr&ograve; chơi: mỗi người chọn 1 con số A v&agrave; B, ai liệt k&ecirc; được nhiều số chia hết cho A hoặc B l&agrave; người chiến thắng<br />
H&atilde;y gi&uacute;p Nam chiến thắng Tiệp bằng c&aacute;ch viết 1 h&agrave;m trả về số thứ N chia hết cho A hoặc B</p>

<p><strong>Input:</strong> N, A, B<br />
<strong>Output:</strong> số thứ N chia hết cho A hoặc B<br />
Kết quả c&oacute; thể rất lớn, trả về kết quả sau khi&nbsp;% (10^9 + 7)</p>

<p><strong>V&iacute; dụ 1:&nbsp;</strong><br />
Input: 1 &nbsp;2 &nbsp;3<br />
Output: 2<br />
Giải th&iacute;ch: c&aacute;c số chia hết c&oacute; 2 hoặc 3: 2,3,4,6,8,9</p>

<p><strong>V&iacute; dụ 2:&nbsp;</strong><br />
Input: 7 5 6<br />
Output: 20<br />
Giải th&iacute;ch : C&aacute;c số chia hết cho 5 hoặc 6: 5,6,10,12,15,18,20,24,25</p>

<p>Note:<br />
LVL I: n , a, b &lt; 100<br />
LVL&nbsp;II: n, a, b &lt; 10^ 9<br />
LVL&nbsp;III: n, a, b &lt; 10^9, time &lt; 0,1s</p><br>*****<br><br>class Solution():<br>    def chia_het_ii(self, n, a, b):<br><br><br>*****<br><br>Test case 1:<br>     Input:   1
2
3
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   41604
3237
3281
<br>     Expected Output:   67801865<br>Test case 3:<br>     Input:   3545
23419
20047
<br>     Expected Output:   38290065<br>Test case 4:<br>     Input:   80303
28146
12988
<br>     Expected Output:   713690600<br><br>*****