<p>Tập [1,2,3, ..., n] chứa n! ho&aacute;n vị độc đ&aacute;o.<br />
&nbsp;<br />
&nbsp;Bằng c&aacute;ch liệt k&ecirc; v&agrave; d&aacute;n nh&atilde;n tất cả c&aacute;c ho&aacute;n vị theo thứ tự, ch&uacute;ng t&ocirc;i nhận được chuỗi sau cho n = 3:<br />
&nbsp;(Theo thứ tự từ điển)<br />
&nbsp;&quot;123&quot;<br />
&nbsp;&quot;132&quot;<br />
&nbsp;&quot;213&quot;<br />
&nbsp;&quot;231&quot;<br />
&nbsp;&quot;312&quot;<br />
&nbsp;&quot;321&quot;<br />
&nbsp;Cho n v&agrave; k, trả về chuỗi ho&aacute;n vị thứ k.<br />
&nbsp;<br />
&nbsp;Ch&uacute; th&iacute;ch:<br />
&nbsp;<br />
&nbsp;Cho n sẽ bao gồm từ 1 đến 9.<br />
&nbsp;Cho k sẽ nằm trong khoảng từ 1 đến n! đ&atilde; bao gồm.<br />
&nbsp;V&iacute; dụ 1:<br />
&nbsp;<br />
&nbsp;Đầu v&agrave;o: n = 3, k = 3<br />
&nbsp;Đầu ra: &quot;213&quot;<br />
&nbsp;V&iacute; dụ 2:<br />
&nbsp;<br />
&nbsp;Đầu v&agrave;o: n = 4, k = 9<br />
&nbsp;Đầu ra: &quot;2314&quot;</p><br>*****<br><br>class Solution():<br>    def hoan_vi_thu_n(self, n, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   3
3
<br>     Expected Output:   "213"<br>Test case 2:<br>     Input:   4
9
<br>     Expected Output:   "2314"<br>Test case 3:<br>     Input:   4
5
<br>     Expected Output:   "1423"<br>Test case 4:<br>     Input:   6
5
<br>     Expected Output:   "123645"<br><br>*****