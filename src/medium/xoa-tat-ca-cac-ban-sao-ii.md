<p>Cho một chuỗi s, loại bỏ c&aacute;c bản sao bị &nbsp;tr&ugrave;ng lặp.Bao gồm chọn k chữ c&aacute;i liền kề nhau v&agrave; giống nhau từ s.Loại bỏ ch&uacute;ng, nối hai b&ecirc;n tr&aacute;i, phải của chuỗi con bị x&oacute;a lại với nhau.<br />
Ch&uacute;ng ta li&ecirc;n tục thực hiện loại bỏ k c&aacute;c từ tr&ugrave;ng lặp cho đến khi tr&ecirc;n s kh&ocirc;ng c&ograve;n c&oacute; thể thực hiện được nữa.<br />
Trả về chuỗi cuối c&ugrave;ng khi thực hiện tất cả c&aacute;c lần loại bỏ tr&ugrave;ng lặp.<br />
Đảm bảo rằng đ&oacute; l&agrave; c&acirc;u trả lời duy nhất.<br />
V&iacute; dụ 1:</p>

<p>Đầu v&agrave;o: s = &quot;abcd&quot;, k = 2<br />
Đầu ra: &quot;abcd&quot;<br />
Giải th&iacute;ch: Kh&ocirc;ng c&oacute; g&igrave; để x&oacute;a.<br />
V&iacute; dụ 2:</p>

<p>Đầu v&agrave;o: s = &quot;deeedbbcccbdaa&quot;, k = 3<br />
Đầu ra: &quot;aa&quot;<br />
Giải th&iacute;ch:<br />
Đầu ti&ecirc;n x&oacute;a &quot;eee&quot; v&agrave; &quot;ccc&quot;, nhận &quot;ddbbbdaa&quot;<br />
Sau đ&oacute; x&oacute;a &quot;bbb&quot;, nhận &quot;dddaa&quot;<br />
Cuối c&ugrave;ng x&oacute;a &quot;ddd&quot;, nhận &quot;aa&quot;&nbsp;</p><br>*****<br><br>class Solution():<br>    def xoa_tat_ca_cac_ban_sao_ii(self, s, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "abcd"
2
<br>     Expected Output:   "abcd"<br>Test case 2:<br>     Input:   "deeedbbcccbdaa"
3
<br>     Expected Output:   "aa"<br>Test case 3:<br>     Input:   "pbbcggttciiippooaais"
2
<br>     Expected Output:   "ps"<br>Test case 4:<br>     Input:   "aaabbccbbbddaa"
3
<br>     Expected Output:   "bbccddaa"<br><br>*****