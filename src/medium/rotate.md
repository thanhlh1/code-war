<p>Một t&ecirc;n trộm may mắn lọt v&agrave;o x&oacute;m nh&agrave; gi&agrave;u ăn trộm.<br />
Mỗi nh&agrave; thứ i c&oacute; arr[i] tiền trong k&eacute;t sắt.&nbsp;<br />
L&agrave; một t&ecirc;n trộm c&oacute; nguy&ecirc;n tắc, hắn quyết định chỉ lấy tiền ở c&aacute;c nh&agrave; liền kề nhau v&agrave; lấy mỗi nh&agrave; một số tiền giống hệt nhau.<br />
H&atilde;y t&iacute;nh xem số tiền tối đa m&agrave; t&ecirc;n trộm c&oacute; thể lấy được.</p>

<p>Input: mảng arr[] chứa số tiền của từng nh&agrave;,&nbsp;<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Số nguy&ecirc;n n: số phần tử của arr (n &lt; 100000)<br />
Output: số nguy&ecirc;n x l&agrave; số tiền tối đa c&oacute; thể lấy được</p>

<p>V&iacute; dụ 1:&nbsp;<br />
Input: [1, 2, 3, 4, 5], 5<br />
Output: 9&nbsp;<br />
Giải th&iacute;ch: c&oacute; thể lấy ở nh&agrave; số 3, 4, 5 mỗi nh&agrave; 3$&nbsp;</p>

<p><br />
V&iacute; dụ 2:<br />
Input: [3,2,9,1,5,5,6,3], 8<br />
Output: 15<br />
Giải th&iacute;ch: lấy ở c&aacute;c nh&agrave; từ 5-&gt;7 mỗi nh&agrave; 5$.</p><br>*****<br><br>class Solution():<br>    def so_tien_toi_da_ii(self, arr, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1, 2, 3, 4, 5]
5
<br>     Expected Output:   9<br>Test case 2:<br>     Input:   [3,2,9,1,5,5,6,3]
8
<br>     Expected Output:   15<br>Test case 3:<br>     Input:   [3, 3794, 9875, 8695, 3656, 271, 5915, 4441, 3936, 7373, 6367, 7251, 3888]
13
<br>     Expected Output:   27216<br>Test case 4:<br>     Input:   [3, 9316, 2275, 633, 4819, 9579, 5896, 5753, 3901, 8634]
10
<br>     Expected Output:   23406<br><br>*****