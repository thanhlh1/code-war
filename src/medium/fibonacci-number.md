<p>Cho một số nguy&ecirc;n dương n, tạo một ma trận vu&ocirc;ng chứa đầy c&aacute;c phần tử từ 1 đến n * n&nbsp;theo thứ tự xoắn ốc.<br />
1&nbsp;&nbsp; &nbsp;2&nbsp;&nbsp; &nbsp;3<br />
8&nbsp;&nbsp; &nbsp;9&nbsp;&nbsp; &nbsp;4<br />
7&nbsp;&nbsp; &nbsp;6&nbsp;&nbsp; &nbsp;5<br />
T&igrave;m vị tr&iacute; của số thứ k trong ma trận xoắn ốc</p>

<p>Input: n, k ( n &lt; 10000, k &lt;= n * n)<br />
Output: mảng gồm 2 phần tử x,y l&agrave; tọa độ của số thứ k trong ma trận</p>

<p><br />
V&iacute; dụ 1:<br />
Input: 5, 15<br />
Output: [3,1]</p>

<p>V&iacute; dụ 2:<br />
Input: 1, 1<br />
Output: [1,1]</p>

<p>V&iacute; dụ 3:<br />
Input: 3, 3<br />
Output: [1,3]</p><br>*****<br><br>class Solution():<br>    def ma_tran_xoan_oc_iii_medium(self, n, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   1
1
<br>     Expected Output:   [1,1]<br>Test case 2:<br>     Input:   5
20
<br>     Expected Output:   [3,4]<br>Test case 3:<br>     Input:   4
13
<br>     Expected Output:   [2,2]<br>Test case 4:<br>     Input:   8532
23022221
<br>     Expected Output:   [7794,7572]<br><br>*****