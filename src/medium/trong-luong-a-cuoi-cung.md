<p>Ch&uacute;ng t&ocirc;i c&oacute; một bộ sưu tập đ&aacute;, mỗi tảng đ&aacute; c&oacute; trọng số nguy&ecirc;n dương.</p>

<p>Mỗi lượt, ch&uacute;ng t&ocirc;i chọn bất kỳ hai h&ograve;n đ&aacute; v&agrave; đập ch&uacute;ng lại với nhau. Giả sử c&aacute;c vi&ecirc;n đ&aacute; c&oacute; trọng số x v&agrave; y với x &lt;= y. Kết quả của sự cố n&agrave;y l&agrave;:</p>

<p>Nếu x == y, cả hai vi&ecirc;n đ&aacute; đều bị ph&aacute; hủy ho&agrave;n to&agrave;n;<br />
Nếu x! = Y, vi&ecirc;n đ&aacute; c&oacute; trọng lượng x bị ph&aacute; hủy ho&agrave;n to&agrave;n v&agrave; vi&ecirc;n đ&aacute; c&oacute; trọng lượng y c&oacute; trọng lượng mới y-x.<br />
Cuối c&ugrave;ng, c&oacute; nhiều nhất 1 vi&ecirc;n đ&aacute; c&ograve;n lại. Trả lại trọng lượng nhỏ nhất c&oacute; thể của vi&ecirc;n đ&aacute; n&agrave;y (trọng lượng bằng 0 nếu kh&ocirc;ng c&ograve;n vi&ecirc;n đ&aacute; n&agrave;o.)</p>

<p>Example 1:</p>

<p>Input: [2,7,4,1,8,1]<br />
Output: 1<br />
Explanation:&nbsp;<br />
ta kết hợp 2 and 4 để lấy được 2 sau đ&oacute; mảng cuối c&ugrave;ng l&agrave; [2,7,1,8,1].<br />
ta kết hợp 7 and 8 để lấy được 1 sau đ&oacute; mảng cuối c&ugrave;ng l&agrave; [2,1,1,1].<br />
ta kết hợp 2 and 1 để lấy được 1 sau đ&oacute; mảng cuối c&ugrave;ng l&agrave; [1,1,1].<br />
ta kết hợp 1 and 1 để lấy được 0 sau đ&oacute; mảng cuối c&ugrave;ng l&agrave; [1].<br />
&nbsp;</p>

<p>Note:</p>

<p>1 &lt;= stones.length &lt;= 30<br />
1 &lt;= stones[i] &lt;= 100</p><br>*****<br><br>class Solution():<br>    def trong_luong_da_cuoi_cung(self, var1):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [2,7,4,1,8,1]
<br>     Expected Output:   1<br>Test case 2:<br>     Input:   [1, 2]
<br>     Expected Output:   1<br>Test case 3:<br>     Input:   [1, 2, 4, 5, 6,7, 2, 3, 5]
<br>     Expected Output:   1<br>Test case 4:<br>     Input:   [3, 4, 5, 7, 10, 11, 13, 16, 17, 23, 25, 26, 27, 28, 29, 30, 32, 35, 37, 38, 41, 50, 53]
<br>     Expected Output:   0<br><br>*****