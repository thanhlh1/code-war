<p>Nam v&agrave; Tiệp chạy đua tr&ecirc;n s&acirc;n b&oacute;ng mxn được chia th&agrave;nh c&aacute;c &ocirc;.&nbsp;<br />
Bắt đầu từ &ocirc; tr&ecirc;n c&ugrave;ng b&ecirc;n tr&aacute;i, đ&iacute;ch l&agrave; &ocirc; dưới c&ugrave;ng b&ecirc;n phải.<br />
Mỗi lần di chuyển 1 &ocirc;, chỉ được ph&eacute;p đi sang phải hoặc xuống dưới.<br />
H&atilde;y gi&uacute;p Nam v&agrave;&nbsp;Tiệp đếm tất cả c&aacute;c c&aacute;ch đi tới đ&iacute;ch</p>

<p>Input: m, n : int<br />
Output: result: số c&aacute;ch di chuyển tới đ&iacute;ch.</p>

<p>Lưu &yacute;: Kết quả đầu ra l&agrave; kết quả sau khi chia dư cho 10^9 + 7.</p>

<p><br />
V&iacute; dụ 1:<br />
Input: m = 2, n = 2<br />
Output: 2<br />
Giải th&iacute;ch: c&oacute; 2 c&aacute;ch đi: Sang phải trước rồi đi xuống, hoặc đi xuống trước rồi sang phải</p>

<p>V&iacute; dụ 2:<br />
Input: m = 5, n = 4<br />
Output: 35</p>

<p>V&iacute; dụ 3:<br />
Input: m = 1, n = 1<br />
Output: 1<br />
Giải th&iacute;ch: c&oacute; 1 c&aacute;ch&nbsp;l&agrave; đứng im :v&nbsp;</p><br>*****<br><br>class Solution():<br>    def duong_ve_dich(self, m, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   2
2
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   5
4
<br>     Expected Output:   35<br>Test case 3:<br>     Input:   1
2
<br>     Expected Output:   1<br>Test case 4:<br>     Input:   10
9
<br>     Expected Output:   24310<br><br>*****