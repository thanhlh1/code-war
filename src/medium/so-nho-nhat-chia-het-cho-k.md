<p>Nam v&agrave; Tiệp đang học đại số, c&oacute; một b&agrave;i to&aacute;n kh&oacute; qu&aacute; m&agrave; cả 2 đều kh&ocirc;ng giải được.&nbsp;<br />
Thầy gi&aacute;o đưa cho 2 bạn một con số K, y&ecirc;u cầu t&igrave;m ra N nhỏ nhất sao cho số tạo bởi N chữ số 1 li&ecirc;n tiếp&nbsp;chia hết cho K.<br />
Đ&atilde; gần đến giờ nộp b&agrave;i nhưng 2 bạn vẫn chưa c&oacute; &yacute; tưởng g&igrave;.&nbsp;<br />
H&atilde;y gi&uacute;p 2 bạn ấy t&igrave;m ra đ&aacute;p &aacute;n.&nbsp;</p>

<p>Input: K<br />
Output: N<br />
Nếu kh&ocirc;ng tồn tại số n&agrave;o, trả về -1</p>

<p>V&iacute; dụ 1:<br />
Input: 1<br />
Output: 1<br />
C&acirc;u trả lời nhỏ nhất l&agrave; N = 1, c&oacute; độ d&agrave;i 1.</p>

<p>V&iacute; dụ 2:<br />
Input: 2<br />
Output: -1.&nbsp;<br />
Số tạo bởi N số 1 lu&ocirc;n kh&ocirc;ng chia hết cho 2</p>

<p>V&iacute; dụ 3:<br />
Input: 3<br />
Output: 3<br />
Giải th&iacute;ch: c&acirc;u trả lời nhỏ nhất l&agrave; 111 chia hết cho 3</p>

<p>Ghi ch&uacute; : 1 &lt;= K &lt;= 10^8</p><br>*****<br><br>class Solution():<br>    def day_so_1(self, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   1
<br>     Expected Output:   1<br>Test case 2:<br>     Input:   9
<br>     Expected Output:   9<br>Test case 3:<br>     Input:   10
<br>     Expected Output:   -1<br>Test case 4:<br>     Input:   11
<br>     Expected Output:   2<br><br>*****