<p>Nam startup một dự &aacute;n nhỏ nhưng do tin tưởng Tiệp n&ecirc;n cho Tiệp tự lo to&agrave;n bộ phần backend. Do trẻ người non dạ, chưa trải đời nhiều n&ecirc;n path api viết rất lộn xộn.<br />
Nam khi nhận được api th&igrave; rất tức giận v&agrave; y&ecirc;u cầu Tiệp Phải x&oacute;a to&agrave;n bộ mục con đi trong c&aacute;c path đi.<br />
Tiệp định x&oacute;a tay nhưng khi nh&igrave;n lại th&igrave; c&oacute; rất nhiều url n&ecirc;n định viết tool để x&oacute;a cho nhanh.</p>

<p>Nếu một mục [i] nằm trong một mục&nbsp;[j] th&igrave; [i] được gọi l&agrave; thư mục con của [j].<br />
Đầu v&agrave;o l&agrave; đường dẫn c&aacute;c url c&oacute; định dạng l&agrave; một hoặc nhiều chuỗi k&yacute; tự c&oacute; dạng: &quot;/&quot;&nbsp;+&nbsp;một hoặc nhiều chữ c&aacute;i tiếng Anh viết thường.&nbsp;<br />
V&iacute; dụ: &quot;/ricode/contest&quot; l&agrave; chuỗi url hợp lệ, chuỗi trống hoặc chỉ c&oacute; / th&igrave; kh&ocirc;ng hợp lệ.</p>

<p><strong>V&iacute; dụ 1:</strong><br />
Input: &nbsp;[&quot;/ricode&quot;,&quot;/ricode/contest&quot;]<br />
Output: &nbsp;[&quot;/ricode&quot;]<br />
Giải th&iacute;ch: [&quot;/ricode/contest&quot;] l&agrave;&nbsp;mục con của [&quot;/ricode&quot;]</p>

<p><strong>V&iacute; dụ 2:&nbsp;</strong><br />
Input: &nbsp;[&quot;/r&quot;,&quot;/aa&quot;,&quot;/a/r&quot;]<br />
Output: &nbsp;[&quot;/r&quot;,&quot;/aa&quot;,&quot;/a/r&quot;]<br />
Giải th&iacute;ch: kh&ocirc;ng c&oacute; mục n&agrave;o l&agrave; con của mục n&agrave;o.</p>

<p><strong>C&aacute;c r&agrave;ng buộc:</strong><br />
&nbsp; &nbsp; 1 &lt;= url.length &lt;= 4 * 10 ^ 4<br />
&nbsp; &nbsp; 2 &lt;= url[i].length &lt;= 100<br />
&nbsp; &nbsp; url[i] chỉ chứa c&aacute;c chữ c&aacute;i thường v&agrave;&nbsp; &#39;/&#39;<br />
&nbsp; &nbsp; url[i] lu&ocirc;n bắt đầu bằng k&yacute; tự &#39;/&#39;<br />
&nbsp; &nbsp; Mỗi t&ecirc;n&nbsp;mục l&agrave; duy nhất.</p><br>*****<br><br>class Solution():<br>    def don_dep(self, url):<br><br><br>*****<br><br>Test case 1:<br>     Input:   ["/ricode","/ricode/contest"]
<br>     Expected Output:   ["/ricode"]<br>Test case 2:<br>     Input:   ["/a","/a/b","/c/d","/c/d/e","/c/f"]
<br>     Expected Output:   ["/a","/c/d","/c/f"]<br>Test case 3:<br>     Input:   ["/a","/a/b","/c/d/e/g/h","/c/d/e/g","/c/f"]
<br>     Expected Output:   ["/a","/c/d/e/g","/c/f"]<br>Test case 4:<br>     Input:   ["/a"]
<br>     Expected Output:   ["/a"]<br><br>*****