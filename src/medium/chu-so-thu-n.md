<p>T&igrave;m chữ số thứ n của d&atilde;y số nguy&ecirc;n v&ocirc; hạn 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ...</p>

<p>Lưu &yacute;:</p>

<p>n l&agrave; dương v&agrave; sẽ nằm trong phạm vi của số nguy&ecirc;n c&oacute; chữ k&yacute; 32 bit ( n &lt;2^31 ).</p>

<p>V&iacute; dụ 1:</p>

<p>&nbsp; &nbsp; Input: 3</p>

<p>&nbsp; &nbsp;Output: 3</p>

<p>V&iacute; dụ 2:</p>

<p>&nbsp; &nbsp; Input: 11</p>

<p>&nbsp; &nbsp; Output: 0</p>

<p>&nbsp; &nbsp; Giải tr&igrave;nh: Chữ số thứ 11 của d&atilde;y 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ... l&agrave; một số 0, l&agrave; một phần của số 10</p>

<p>V&iacute; dụ 3</p>

<p>&nbsp; &nbsp; Input: 12</p>

<p>&nbsp; &nbsp; Output: 1</p>

<p>&nbsp; &nbsp; Giải tr&igrave;nh: Chữ số thứ 12 của d&atilde;y 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ... l&agrave; một số 1, l&agrave; một phần của số 11</p><br>*****<br><br>class Solution():<br>    def chu_so_thu_n(self, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   3
<br>     Expected Output:   3<br>Test case 2:<br>     Input:   439073
<br>     Expected Output:   3<br>Test case 3:<br>     Input:   20379
<br>     Expected Output:   3<br>Test case 4:<br>     Input:   341582
<br>     Expected Output:   5<br><br>*****