<p>Cho mảng c&aacute;c số nguy&ecirc;n nums[]<br />
H&atilde;y t&igrave;m xem c&oacute; bao nhi&ecirc;u tổng tạo th&agrave;nh từ c&aacute;c tập hợp con của nums.</p>

<p>Input: nums[] mảng c&aacute;c số nguy&ecirc;n.&nbsp;<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;nums c&oacute; &iacute;t hơn 30 &nbsp;phần tử</p>

<p>Output: Integer&nbsp;</p>

<p>V&iacute; dụ:&nbsp;<br />
Input: nums = [1,3,3]</p>

<p>0 (tập rỗng)<br />
1<br />
3<br />
1+3 = 4<br />
3+3 = 6<br />
1+3+3 = 7<br />
Output: 6<br />
Giải th&iacute;ch: C&aacute;c tổng tạo được l&agrave;: 0,1,3,4,6,7</p>

<p>V&iacute; dụ 2<br />
Input: nums = [1,2,3]</p>

<p>0 (tập rỗng)<br />
1<br />
2<br />
3<br />
1 + 2 = 3<br />
1+3 = 4<br />
2+3 = 5<br />
1+2+3 = 6<br />
Output: 7&nbsp;<br />
Giải th&iacute;ch: C&aacute;c tổng tạo được l&agrave;: 0,1,2,3,4,5,6</p><br>*****<br><br>class Solution():<br>    def tong_cac_so_3_medium(self, nums):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3]
<br>     Expected Output:   7<br>Test case 2:<br>     Input:   [1,3,3]
<br>     Expected Output:   6<br>Test case 3:<br>     Input:   [1,3,4]
<br>     Expected Output:   7<br>Test case 4:<br>     Input:   [2286,94652,44339,56950,19056,61417]
<br>     Expected Output:   64<br><br>*****