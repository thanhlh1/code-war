<p>X&acirc;u fibonaci được x&acirc;y dựng như sau:&nbsp;<br />
Cho 2 x&acirc;u cơ sở: x,y, c&aacute;c x&acirc;u fibonaci được sinh ra bằng c&aacute;ch nối 2 x&acirc;u fibonaci trước đ&oacute;: f(n) = f(n-2) + f(n-1)&nbsp;<br />
V&iacute; dụ: f(0) = a, f(1) = bc&nbsp;<br />
C&aacute;c x&acirc;u fibonaci tiếp theo:&nbsp;<br />
&nbsp;&nbsp; &nbsp;f(2) = f(0) + f(1) = abc,&nbsp;<br />
&nbsp;&nbsp; &nbsp;f(3) = f(1) + f(2) = bcabc<br />
&nbsp;&nbsp; &nbsp;f(4) = f(2) + f(3) = abcbcabc</p>

<p><strong>B&agrave;i to&aacute;n</strong>: Cho 2 x&acirc;u cở sở str1, str2. T&igrave;m k&iacute; tự&nbsp;thứ k trong x&acirc;u fibonaci thứ n.<br />
Input: str1, str2, n, k&nbsp; (n &lt; 100, k &lt; len(f(n), len(str1) &lt; 10, len(str2) &lt; 10)</p>

<p>Output: k&iacute; tự thứ k trong f(n)</p>

<p><strong>V&iacute; dụ:&nbsp;</strong><br />
&nbsp;&nbsp; &nbsp;Input: str1 = &quot;a&quot;, str2 = &quot;bc&quot;, n = 5, k = 9<br />
&nbsp;&nbsp; &nbsp;Output: &quot;b&quot;<br />
&nbsp;&nbsp; &nbsp;Giải th&iacute;ch: f(0) = a, f(1) = bc, f(2) = abc, f(3) = bcabc, f(4) = abcbcabc, f(5) = bcabcabcbcabc<br />
&nbsp;&nbsp; &nbsp;K&iacute; tự thứ 9 của f(5) l&agrave; &quot;b&quot;&nbsp;</p>

<p><br />
<strong>V&iacute; dụ 2:</strong><br />
&nbsp;&nbsp; &nbsp;Input: str1 = &quot;a&quot;, str2 = &quot;abc&quot;, n = 3, k = 5<br />
&nbsp;&nbsp; &nbsp;Output: &quot;a&quot;<br />
&nbsp;&nbsp; &nbsp;Giải th&iacute;ch: f(0) = &quot;a&quot;, f(1) = &quot;abc&quot;, f(2) = &quot;aabc&quot;, f(3) = &quot;abcaabc&quot;<br />
&nbsp;&nbsp; &nbsp;K&iacute; tự thứ 5 của f(3) l&agrave; &quot;a&quot;</p><br>*****<br><br>class Solution():<br>    def chuoi_fibonaci_ii(self, str1, str2, n, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "kI"
"MK"
4
4
<br>     Expected Output:   "K"<br>Test case 2:<br>     Input:   "Wu"
"as"
5
13
<br>     Expected Output:   "W"<br>Test case 3:<br>     Input:   cLcrS
cIaMMA
60
525400430
<br>     Expected Output:   "L"<br>Test case 4:<br>     Input:   "oF"
"DSyZGPYMoF"
30
755
<br>     Expected Output:   "D"<br><br>*****