<p>Tiệp l&agrave; cửu vạn ở bến Thượng Hải, h&ocirc;m nay c&oacute; chuyến h&agrave;ng lớn của anh Cường cập bến, Tiệp phải chuyển gấp trong v&ograve;ng D giờ.<br />
Tuy nhi&ecirc;n, Tiệp kh&ocirc;ng đủ sức để mang hết 1 lần, mỗi lần chuyển h&agrave;ng phải mất 1h đi lại<br />
Để giữ sức đi chơi với người y&ecirc;u, mỗi lần Tiệp chỉ mang tối đa X kg mỗi lần.&nbsp;<br />
C&aacute;c bạn h&atilde;y gi&uacute;p Tiệp t&igrave;m X để vừa chuyển h&agrave;ng kịp giờ, vừa đỡ mệt nhất c&oacute; thể<br />
(Phải mang&nbsp;c&aacute;c th&ugrave;ng theo đ&uacute;ng thứ tự cho trước)</p>

<p>Input: mảng c&aacute;c số nguy&ecirc;n weight&nbsp;thể hiện số&nbsp;c&acirc;n nặng của từng th&ugrave;ng h&agrave;ng<br />
Số nguy&ecirc;n&nbsp;thể hiện số&nbsp;giờ D d&agrave;nh cho Tiệp<br />
Output: X</p>

<p>V&iacute; dụ: &nbsp;[1,2,3,4,5,6,7,8,9,10], D = 5<br />
Output: 15<br />
Giải th&iacute;ch: Tiệp mang 5 lần :&nbsp;<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Lần 1: 1,2,3,4,5<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Lần 2: 6,7<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Lần 3: 8<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Lần 4: 9<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Lần 5: 10</p>

<p>V&iacute; dụ 2:[1,2,3,1,1], 4<br />
Output : 3<br />
&nbsp;&nbsp; &nbsp;Lần 1: 1<br />
&nbsp;&nbsp; &nbsp;Lần 2: 2<br />
&nbsp;&nbsp; &nbsp;Lần 3: 3<br />
&nbsp; &nbsp; Lần 4: 1,1<br />
&nbsp;&nbsp; &nbsp;<br />
*Note: 1 &lt;= D &lt;= 50000<br />
Mỗi th&ugrave;ng nặng tối đa 500 kg, kh&ocirc;ng th&ugrave;ng&nbsp;n&agrave;o rỗng</p><br>*****<br><br>class Solution():<br>    def chuyen_hang(self, weight, d):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3,4,5,6,7,8,9,10]
5
<br>     Expected Output:   15<br>Test case 2:<br>     Input:   [3,2,2,4,1,4]
3
<br>     Expected Output:   6<br>Test case 3:<br>     Input:   [1,2,3,1,1]
4
<br>     Expected Output:   3<br>Test case 4:<br>     Input:   [1,5,8,9,15,16,18,23,24,25]
6
<br>     Expected Output:   31<br><br>*****