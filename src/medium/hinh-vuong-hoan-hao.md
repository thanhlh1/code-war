<p>Một phần thưởng c&oacute; gi&aacute; trị l&agrave; n đồng v&agrave;ng, t&igrave;m số người &iacute;t nhất để trao giải thưởng n&agrave;y sao cho:&nbsp;<br />
- Mỗi người được trao x đồng v&agrave;ng, x l&agrave; số ch&iacute;nh phương (1,4,9,16,25 ....)<br />
- Phần thưởng phải được trao vừa hết</p>

<p>Đầu v&agrave;o: n: int. n &gt;= 0</p>

<p>Đầu ra: Số người &iacute;t nhất nhận thưởng.<br />
<strong>V&iacute; dụ 1:</strong><br />
Input: n = 25;<br />
Output: 1</p>

<p><strong>V&iacute; dụ 2:</strong><br />
Input: n = 11<br />
Output: 3<br />
Giải th&iacute;ch: 11 = 1 + 1&nbsp;+ 9.</p>

<p>&nbsp;</p><br>*****<br><br>class Solution():<br>    def giai_thuong_hoan_hao(self, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   25
<br>     Expected Output:   1<br>Test case 2:<br>     Input:   11
<br>     Expected Output:   3<br>Test case 3:<br>     Input:   10
<br>     Expected Output:   2<br>Test case 4:<br>     Input:   190
<br>     Expected Output:   3<br><br>*****