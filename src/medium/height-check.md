<p>Cho mảng c&aacute;c số nguy&ecirc;n gồm n phần tử<br />
Với mỗi nh&oacute;m k phần tử li&ecirc;n tiếp chọn ra 1 số nhỏ nhất<br />
Trong c&aacute;c số vừa chọn được lại tiếp tục chọn ra số lớn nhất X<br />
H&atilde;y t&igrave;m X</p>

<p>Input: arr[] mảng số nguy&ecirc;n gồm n phần tử,&nbsp;<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;số nguy&ecirc;n k (k &lt;= n)<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;số nguy&ecirc;n n &nbsp;<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;(n, k &lt; 100000)<br />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;<br />
Output: số nguy&ecirc;n X</p>

<p>V&iacute; dụ 1:&nbsp;<br />
Input:[1,3,5,4,2,9,6,7,2], 3, 9<br />
Output: 6<br />
Giải th&iacute;ch: 6 l&agrave; số nhỏ nhất trong nh&oacute;m [9,6,7]</p>

<p>V&iacute; dụ 2:<br />
Input:[1,11,10,4,2,9,6,7,2], 3, 9<br />
Output: 6<br />
Giải th&iacute;ch: 6 l&agrave; số nhỏ nhất trong nh&oacute;m [9,6,7]</p><br>*****<br><br>class Solution():<br>    def nho_nhat_va_lon_nhat_ii(self, arr, k, n):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,8,1,0,1,9,9,9]
3
8
<br>     Expected Output:   9<br>Test case 2:<br>     Input:   [1,3,5,4,2,9,6,7,2]
3
9
<br>     Expected Output:   6<br>Test case 3:<br>     Input:   [1,11,10,4,2,9,6,7,2]
3
9
<br>     Expected Output:   6<br>Test case 4:<br>     Input:   [10, 5, 4, 13, 8, 7, 16, 8, 3, 6, 1, 16, 19, 8, 17, 3, 20, 17, 6, 3, 16, 9, 8, 10, 19, 4, 4, 3, 6, 5]
7
30
<br>     Expected Output:   4<br><br>*****