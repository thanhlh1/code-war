<p>Bạn của bạn đang g&otilde; t&ecirc;n của m&igrave;nh v&agrave;o b&agrave;n ph&iacute;m. Thỉnh thoảng, khi g&otilde; một k&yacute; tự bất k&igrave;, ph&iacute;m c&oacute; thể bị nhấn l&acirc;u v&agrave; k&yacute; tự sẽ được g&otilde; 1 hoặc nhiều lần.<br />
&nbsp;<br />
&nbsp;Bạn kiểm tra c&aacute;c k&yacute; tự g&otilde; của b&agrave;n ph&iacute;m. Trả về true nếu c&oacute; thể đ&oacute; l&agrave; t&ecirc;n&nbsp;của bạn, với một số k&yacute; tự (c&oacute; thể kh&ocirc;ng c&oacute;) được nhấn l&acirc;u.<br />
&nbsp;<strong>Đầu v&agrave;o: </strong></p>

<p>&nbsp; &nbsp; name: String : Một chuỗi l&agrave; t&ecirc;n của bạn.</p>

<p>&nbsp; &nbsp; typed: String: Một chuỗi l&agrave; kết quả của việc g&otilde; t&ecirc;n bạn v&agrave;o b&agrave;n ph&iacute;m.&nbsp;&nbsp;<br />
&nbsp;&nbsp;<br />
&nbsp;<strong>Đầu ra</strong>: true/false: kết quả của b&agrave;i to&aacute;n.<br />
<strong>&nbsp;V&iacute; dụ 1:</strong><br />
&nbsp;<br />
&nbsp;Đầu v&agrave;o: name = &quot;global&quot;, typed = &quot;gloobaal&quot;<br />
&nbsp;Đầu ra: true<br />
&nbsp;Giải th&iacute;ch: &#39;o&#39; v&agrave; &#39;a&#39; trong &#39;global&#39; đ&atilde; bị&nbsp;nhấn l&acirc;u.<br />
<strong>&nbsp;V&iacute; dụ</strong> <strong>2</strong>:<br />
&nbsp;<br />
&nbsp;Đầu v&agrave;o: name = &quot;divission&quot;, typed = &quot;divisionnnnnnnn&quot;<br />
&nbsp;Đầu ra: false<br />
&nbsp;Giải th&iacute;ch: &#39;s&#39; phải được nhấn hai lần, nhưng n&oacute; kh&ocirc;ng nằm trong kết quả đầu ra được g&otilde;.<br />
<br />
&nbsp;<strong>V&iacute; dụ 3:</strong><br />
&nbsp;<br />
&nbsp;Đầu v&agrave;o: name = &quot;Namdz&quot;, typed = &quot;Namdz&quot;<br />
&nbsp;Đầu ra: true<br />
&nbsp;Giải th&iacute;ch: Kh&ocirc;ng c&oacute; k&iacute; tự n&agrave;o bị đ&egrave; l&acirc;u.<br />
&nbsp;&nbsp;<br />
&nbsp;<br />
&nbsp;Ch&uacute; th&iacute;ch:<br />
&nbsp;<br />
&nbsp;name .length &lt;= 1000<br />
&nbsp;typed.length &lt;= 1000<br />
&nbsp;C&aacute;c k&yacute; tự của t&ecirc;n v&agrave; đ&aacute;nh m&aacute;y l&agrave; chữ thường.</p><br>*****<br><br>class Solution():<br>    def nhap_sai_ten(self, name, typed):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "global"
"gloobaal"
<br>     Expected Output:   true<br>Test case 2:<br>     Input:   "divission"
"divisionnnnnnnn"
<br>     Expected Output:   false<br>Test case 3:<br>     Input:   "leelee"
"lleeelee"
<br>     Expected Output:   true<br>Test case 4:<br>     Input:   "laiden"
"laiden"
<br>     Expected Output:   true<br><br>*****