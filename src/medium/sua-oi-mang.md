<p>Bạn được giao một b&agrave;i to&aacute;n như sau:<br />
Cho một mảng A gồm c&aacute;c số nguy&ecirc;n v&agrave; c&aacute;c thao t&aacute;c được ph&eacute;p tr&ecirc;n mảng đ&oacute;: đổi dấu một số bất k&igrave;<br />
Bạn&nbsp;được giao cho K thao t&aacute;c v&agrave; y&ecirc;u cầu phải d&ugrave;ng hết ch&uacute;ng.<br />
Bạn h&atilde;y&nbsp;t&igrave;m ra được c&aacute;ch biến đổi sao cho tổng c&aacute;c số trong mảng l&agrave; lớn nhất.</p>

<p>V&iacute; dụ 1:</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu v&agrave;o: A = [-3, -2, 3, 4, 5, 7, 12, 19, 20, 21], K = 1<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ra: 92<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Giải th&iacute;ch: Chọn&nbsp;chỉ số (1,) v&agrave; A trở th&agrave;nh [3, -2, 3, 4, 5, 7, 12, 19, 20, 21]<br />
V&iacute; dụ 3:</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu v&agrave;o: A =[4,2,3], K = 1<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ra: 5<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Giải th&iacute;ch: Chọn&nbsp;chỉ số (1) v&agrave; A trở th&agrave;nh [4,-2,3]</p>

<p>Ghi ch&uacute;:</p>

<p>1 &lt;= A.length &lt;= 10000<br />
1 &lt;= K &lt;= 10000<br />
-100 &lt;= A [i] &lt;= 100</p><br>*****<br><br>class Solution():<br>    def tong_cac_phan_tu_lon_nhat(self, a, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [-3, -2, 3, 4, 5, 7, 12, 19, 20, 21]
1
<br>     Expected Output:   92<br>Test case 2:<br>     Input:   [4,2,3]
1
<br>     Expected Output:   5<br>Test case 3:<br>     Input:   [1, 5, 8, 10, 15, 18, 20, 21, 23, 25]
1
<br>     Expected Output:   144<br>Test case 4:<br>     Input:   [-5, -4, 1, 3, 4, 7, 11, 14, 19, 20]
5
<br>     Expected Output:   86<br><br>*****