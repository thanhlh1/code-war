<p>Shinichi muốn viết thư cho Ran nhưng lại kh&ocirc;ng muốn để ai kh&aacute;c đọc được n&ecirc;n đ&atilde; m&atilde; h&oacute;a bức thư bằng c&aacute;ch sử dụng &aacute;nh xạ sau:&nbsp;</p>

<ul>
	<li>&#39;A&#39; -&gt; 1</li>
	<li>&#39;B&#39; -&gt; 2</li>
	<li>...</li>
	<li>&#39;Z&#39; -&gt; 26</li>
</ul>

<p>Cho một chuỗi kh&aacute;c rỗng chỉ chứa c&aacute;c chữ số, gi&uacute;p Ran t&igrave;m ra số c&aacute;ch để giải m&atilde; bức thư của Shinichi.</p>

<p><strong>V&iacute; dụ 1:&nbsp;</strong><br />
<em>Input</em>: &nbsp;&quot;20253&quot;<br />
<em>Output</em>: &nbsp;2<br />
Giải th&iacute;ch: &nbsp;C&oacute; thể giải m&atilde; l&agrave; &quot;TBEC&quot; (20 2 5 3) hoặc &quot;TYC&quot; (20 25 3)</p>

<p><strong>V&iacute; dụ 2:&nbsp;</strong><br />
<em>Input</em>: &nbsp;&quot;226&quot;<br />
<em>Output</em>: &nbsp;3<br />
Giải th&iacute;ch: &nbsp;C&oacute; thể giải m&atilde; l&agrave; &quot;BZ&quot; (2 26), &quot;VF&quot; (22 6) hoặc &quot;BBF&quot; (2 2 6)</p><br>*****<br><br>class Solution():<br>    def giai_ma_mat_thu(self, s):<br><br><br>*****<br><br>Test case 1:<br>     Input:   "20253"
<br>     Expected Output:   2<br>Test case 2:<br>     Input:   "226"
<br>     Expected Output:   3<br>Test case 3:<br>     Input:   "112"
<br>     Expected Output:   3<br>Test case 4:<br>     Input:   "2"
<br>     Expected Output:   1<br><br>*****