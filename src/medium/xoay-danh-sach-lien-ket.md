<p>Cho một danh s&aacute;ch được li&ecirc;n kết, xoay danh s&aacute;ch sang phải theo k vị tr&iacute;, trong đ&oacute; k kh&ocirc;ng &acirc;m.<br />
&nbsp;<br />
&nbsp;V&iacute; dụ 1:<br />
&nbsp;<br />
&nbsp;Đầu v&agrave;o: 1-&gt; 2-&gt; 3-&gt; 4-&gt; 5-&gt; NULL, k = 2<br />
&nbsp;Đầu ra: 4-&gt; 5-&gt; 1-&gt; 2-&gt; 3-&gt; NULL<br />
&nbsp;Giải tr&igrave;nh:<br />
&nbsp;xoay 1 bước sang phải: 5-&gt; 1-&gt; 2-&gt; 3-&gt; 4-&gt; NULL<br />
&nbsp;xoay 2 bước sang phải: 4-&gt; 5-&gt; 1-&gt; 2-&gt; 3-&gt; NULL<br />
&nbsp;V&iacute; dụ 2:<br />
&nbsp;<br />
&nbsp;Đầu v&agrave;o: 0-&gt; 1-&gt; 2-&gt; NULL, k = 4<br />
&nbsp;Đầu ra: 2-&gt; 0-&gt; 1-&gt; NULL<br />
&nbsp;Giải tr&igrave;nh:<br />
&nbsp;xoay 1 bước sang phải: 2-&gt; 0-&gt; 1-&gt; NULL<br />
&nbsp;xoay 2 bước sang phải: 1-&gt; 2-&gt; 0-&gt; NULL<br />
&nbsp;xoay 3 bước sang phải: 0-&gt; 1-&gt; 2-&gt; NULL<br />
&nbsp;xoay 4 bước sang phải: 2-&gt; 0-&gt; 1-&gt; NULL</p><br>*****<br><br># class ListNode:<br>#        def __init__(self, x):<br>#            self.val = x<br>#            self.next = None<br>class Solution():<br>    def xoay_danh_sach_lien_ket(self, head, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   [1,2,3,4,5]
2
<br>     Expected Output:   [4,5,1,2,3]<br>Test case 2:<br>     Input:   []
3
<br>     Expected Output:   []<br>Test case 3:<br>     Input:   [1]
3
<br>     Expected Output:   [1]<br><br>*****