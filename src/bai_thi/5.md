<p>Cho hai số nguy&ecirc;n n v&agrave; k, t&igrave;m xem c&oacute; bao nhi&ecirc;u tập hợp kh&aacute;c nhau bao gồm c&aacute;c số từ 1 đến n sao cho c&oacute; ch&iacute;nh x&aacute;c k cặp nghịch đảo.</p>

<p>Định nghĩa một cặp nghịch đảo: Đối với phần tử thứ i v&agrave; thứ j trong mảng a, nếu i &lt;j v&agrave; a [i]&gt; a [j] th&igrave; đ&oacute; l&agrave; một cặp nghịch đảo; Nếu kh&ocirc;ng, n&oacute; kh&ocirc;ng phải l&agrave; cặp nghịch đảo.</p>

<p>V&igrave; c&acirc;u trả lời c&oacute; thể rất lớn, n&ecirc;n c&acirc;u trả lời phải l&agrave; modulo 109 + 7.</p>

<p>V&iacute; dụ 1:</p>

<p>Input: n = 5, k = 1<br />
Output: 4<br />
Giải tr&igrave;nh:<br />
c&aacute;c mảng c&oacute; 1 cặp nghịch đảo: [1,3,2,4,5], [2,1,3,4,5], [1,2,4,3,5], [1,2,3,5,4]<br />
Chỉ mảng [1,2,3] bao gồm c&aacute;c số từ 1 đến 3 c&oacute; ch&iacute;nh x&aacute;c 0 cặp nghịch đảo.</p>

<p><br />
V&iacute; dụ 2:</p>

<p>Input: n = 3, k = 2<br />
Output: 2<br />
Giải tr&igrave;nh:<br />
Mảng [3,1,2] v&agrave; [2,3,1] c&oacute; đ&uacute;ng 2 cặp nghịch đảo.</p>

<p><br />
Ghi ch&uacute;:</p>

<p>Số nguy&ecirc;n n nằm trong phạm vi [1, 1000] v&agrave; k nằm trong phạm vi [0, 1000].</p><br>*****<br><br>class Solution():<br>    def cap_nghich_dao_2(self, n, k):<br><br><br>*****<br><br>Test case 1:<br>     Input:   5
1
<br>     Expected Output:   4<br>Test case 2:<br>     Input:   3
2
<br>     Expected Output:   2<br>Test case 3:<br>     Input:   1
0
<br>     Expected Output:   1<br>Test case 4:<br>     Input:   45
9
<br>     Expected Output:   563495894<br><br>*****